package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreatePlayerDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
public class CreatePlayerView {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserRepository userRepository;


    @GetMapping("/create_player")
    public ModelAndView displayCreatePlayerForm(Authentication authentication) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("create_player");
        String login = authentication.getName();
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User not found!"));
        CreatePlayerDTO createPlayerDTO = new CreatePlayerDTO();
        modelAndView.addObject("createPlayerDTO", createPlayerDTO);

        List<Team> userTeams = new ArrayList<>();
        if (user.getUserTeams() != null) {
            userTeams = user.getUserTeams()
                    .stream()
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("userTeams", userTeams);

        List<String> nationalityList = new ArrayList<>();
        String[] isoCountries = Locale.getISOCountries();
        for (String countryCode : isoCountries) {
            Locale locale = new Locale("", countryCode);
            nationalityList.add(locale.getDisplayCountry(Locale.ENGLISH));
        }
        modelAndView.addObject("nationalityList", nationalityList);
        return modelAndView;
    }

    @PostMapping("/create_player")
    public String createPlayer(@Valid @ModelAttribute CreatePlayerDTO createPlayerDTO, BindingResult bindingResult, Model model, Authentication authentication) throws InvalidData, NotFoundException {
        String login = authentication.getName();
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User not found!"));
        if (bindingResult.hasErrors()) {
            model.addAttribute("createPlayerDTO", createPlayerDTO);

            List<Team> userTeams = new ArrayList<>();
            if (user.getUserTeams() != null) {
                userTeams = user.getUserTeams()
                        .stream()
                        .collect(Collectors.toList());
            }
            model.addAttribute("userTeams", userTeams);

            List<String> nationalityList = new ArrayList<>();
            String[] isoCountries = Locale.getISOCountries();
            for (String countryCode : isoCountries) {
                Locale locale = new Locale("", countryCode);
                nationalityList.add(locale.getDisplayCountry(Locale.ENGLISH));
            }
            model.addAttribute("nationalityList", nationalityList);
            return "create_player";
        }
        try {
            playerService.createPlayer(createPlayerDTO, login);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/player_already_exists_error";
        }
        return "redirect:/user_players?id=" + user.getId();
    }
}
