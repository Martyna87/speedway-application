package com.example.speedway.application.view;

import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.UpdatePlayerDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.PlayerRepository;
import com.example.speedway.application.service.PlayerService;
import com.example.speedway.application.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
public class UpdatePlayerView {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;
    @Autowired
    private TeamService teamService;

    @GetMapping("/update_player")
    public ModelAndView displayUpdatePlayerView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("update_player_form");
        modelAndView.addObject("id", id);

        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this player!"));
        PlayerDTO playerDTO = playerDTOMapper.toDto(player);
        User user = player.getTeam().getUser();
        UpdatePlayerDTO updatePlayerDTO = new UpdatePlayerDTO();
        updatePlayerDTO.setFirstName(playerDTO.getFirstName());
        updatePlayerDTO.setLastName(playerDTO.getLastName());
        updatePlayerDTO.setNickName(playerDTO.getNickName());
        updatePlayerDTO.setBirthYear(playerDTO.getBirthYear());
        updatePlayerDTO.setNationality(playerDTO.getNationality());
        modelAndView.addObject("updatePlayerDTO", updatePlayerDTO);

        List<String> teamNames = user.getUserTeams()
                .stream()
                .map(t -> t.getName())
                .collect(Collectors.toList());
        modelAndView.addObject("teamNames", teamNames);

        List<String> nationalityList = new ArrayList<>();
        String[] isoCountries = Locale.getISOCountries();
        for (String countryCode : isoCountries) {
            Locale locale = new Locale("", countryCode);
            nationalityList.add(locale.getDisplayCountry(Locale.ENGLISH));
        }
        modelAndView.addObject("nationalityList", nationalityList);
        return modelAndView;
    }

    @PostMapping("/update_player/{id}")
    public String updatePlayer(@PathVariable Long id, @Valid @ModelAttribute UpdatePlayerDTO updatePlayerDTO, BindingResult bindingResult, Model model) throws NotFoundException, InvalidData {
        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Player not found!"));
        if (bindingResult.hasErrors()) {
            model.addAttribute("updatePlayerDTO", updatePlayerDTO);
            List<String> teamNames = teamService.getAllTeams().stream()
                    .map(t -> t.getName())
                    .collect(Collectors.toList());
            model.addAttribute("teamNames", teamNames);

            List<String> nationalityList = new ArrayList<>();
            String[] isoCountries = Locale.getISOCountries();
            for (String countryCode : isoCountries) {
                Locale locale = new Locale("", countryCode);
                nationalityList.add(locale.getDisplayCountry(Locale.ENGLISH));
            }
            model.addAttribute("nationalityList", nationalityList);
            return "update_player_form";
        }
        try {
            playerService.updatePlayer(id, updatePlayerDTO);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/player_already_exists_update_error/" + player.getId();
        } catch
        (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }
        return "redirect:/player_details?id={id}";
    }
}
