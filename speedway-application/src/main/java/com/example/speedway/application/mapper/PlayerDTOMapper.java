package com.example.speedway.application.mapper;

import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.model.Player;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlayerDTOMapper {
    public PlayerDTO toDto(Player player) {
        Long teamId = null;
        if (player.getTeam() != null) {
            teamId = player.getTeam()
                    .getId();
        }

        List<Long> redHelmetHostsPlayerRaceIds = null;
        if (player.getRedHelmetHostsPlayerRaces() != null) {
            redHelmetHostsPlayerRaceIds = player.getRedHelmetHostsPlayerRaces()
                    .stream()
                    .map(r -> r.getId())
                    .collect(Collectors.toList());
        }

        List<Long> blueHelmetHostsPlayerRaceIds = null;
        if (player.getBlueHelmetHostsPlayerRaces() != null) {
            blueHelmetHostsPlayerRaceIds = player.getBlueHelmetHostsPlayerRaces()
                    .stream()
                    .map(r -> r.getId())
                    .collect(Collectors.toList());
        }

        List<Long> whiteHelmetGuestsPlayerRaceIds = null;
        if (player.getWhiteHelmetGuestsPlayerRaces() != null) {
            whiteHelmetGuestsPlayerRaceIds = player.getWhiteHelmetGuestsPlayerRaces()
                    .stream()
                    .map(r -> r.getId())
                    .collect(Collectors.toList());
        }

        List<Long> yellowHelmetGuestsPlayerRaceIds = null;
        if (player.getYellowHelmetGuestsPlayerRaces() != null) {
            yellowHelmetGuestsPlayerRaceIds = player.getYellowHelmetGuestsPlayerRaces()
                    .stream()
                    .map(r -> r.getId())
                    .collect(Collectors.toList());
        }

        return PlayerDTO.builder()
                .id(player.getId())
                .firstName(player.getFirstName())
                .lastName(player.getLastName())
                .nickName(player.getNickName())
                .birthYear(player.getBirthYear())
                .nationality(player.getNationality())
                .teamId(teamId)
                .redHelmetHostsPlayerRaceIds(redHelmetHostsPlayerRaceIds)
                .blueHelmetHostsPlayerRaceIds(blueHelmetHostsPlayerRaceIds)
                .whiteHelmetGuestsPlayerRaceIds(whiteHelmetGuestsPlayerRaceIds)
                .yellowHelmetGuestsPlayerRaceIds(yellowHelmetGuestsPlayerRaceIds)
                .build();
    }
}
