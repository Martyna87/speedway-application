package com.example.speedway.application.view;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserAlreadyExistsErrorView {
    @GetMapping("/user_already_exists_error")
    public ModelAndView errorCreateUserView() {
        ModelAndView modelAndView = new ModelAndView("user_already_exists_error");
        return modelAndView;
    }
}
