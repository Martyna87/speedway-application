package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateUpdateTeamDTO {
    @NotBlank(message = "Name must be filled")
    @Size(min = 10, message = "Name must be at least 10 chars long")
    private String name;
    @NotBlank(message = "City must be filled")
    @Size(min = 3, message = "City must be at least 3 chars long")
    private String city;
    @NotBlank(message = "Coach must be filled")
    @Size(min = 3, message = "Coach must be at least 3 chars long")
    private String coach;
}
