package com.example.speedway.application.repository;

import com.example.speedway.application.model.Race;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaceRepository extends JpaRepository<Race, Long> {
}
