package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.repository.RaceRepository;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RaceAlreadyExistsErrorView {
    @Autowired
    GameRepository gameRepository;
    @Autowired
    GameDTOMapper gameDTOMapper;

    @GetMapping("/race_already_exists_error/{gameId}")
    public ModelAndView errorCreateRaceView(@PathVariable Long gameId) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("race_already_exists_error");
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new NotFoundException("Game not found!"));
        GameDTO gameDTO = gameDTOMapper.toDto(game);
        modelAndView.addObject("gameDTO", gameDTO);
        return modelAndView;
    }
}
