package com.example.speedway.application.service;


import com.example.speedway.application.DTO.CreateUpdateRaceDTO;
import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.RaceDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.repository.PlayerRepository;
import com.example.speedway.application.repository.RaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RaceService {
    @Autowired
    private RaceRepository raceRepository;
    @Autowired
    private RaceDTOMapper raceDTOMapper;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private PlayerRepository playerRepository;


    @Transactional
    public List<RaceDTO> getAllRaces() {
        return raceRepository.findAll()
                .stream()
                .map(r -> raceDTOMapper.toDto(r))
                .collect(Collectors.toList());
    }

    @Transactional
    public RaceDTO getRaceById(Long id) throws NotFoundException {
        return raceRepository.findById(id)
                .map(r -> raceDTOMapper.toDto(r))
                .orElseThrow(() -> new NotFoundException("Not found this race!"));
    }

    @Transactional
    public RaceDTO createRace(CreateUpdateRaceDTO createUpdateRaceDTO, Long gameId) throws InvalidData, AlreadyExistsException, NotFoundException {
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new NotFoundException("Not found this game!"));
        validate(createUpdateRaceDTO);

        Player redHelmetHostsPlayer = playerRepository.findById(createUpdateRaceDTO.getRedHelmetHostsPlayerId()).orElseThrow(() -> new NotFoundException("Red player hosts not found!"));
        Player blueHelmetHostsPlayer = playerRepository.findById(createUpdateRaceDTO.getBlueHelmetHostsPlayerId()).orElseThrow(() -> new NotFoundException("Blue helmet hosts player not found!"));
        Player whiteHelmetGuestPlayer = playerRepository.findById(createUpdateRaceDTO.getWhiteHelmetGuestsPlayerId()).orElseThrow(() -> new NotFoundException("White helmet guests player not found!"));
        Player yellowHelmetGuestsPlayer = playerRepository.findById(createUpdateRaceDTO.getYellowHelmetGuestsPlayerId()).orElseThrow(() -> new NotFoundException("Yellow helmet guests player not found!"));
        if (redHelmetHostsPlayer.equals(blueHelmetHostsPlayer) ||
                whiteHelmetGuestPlayer.equals(yellowHelmetGuestsPlayer) ||
                createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() ||
                createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() == createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer()) {
            throw new InvalidData("Players of the same team and points of all players in the race must be different!");
        }

        boolean existsRace = false;
        if (game.getRaces() != null) {
            for (Race gameRace : game.getRaces()) {
                if (gameRace.getNumber() == createUpdateRaceDTO.getNumber()) {
                    existsRace = true;
                }
            }
        }

        if (!existsRace) {
            Race race = Race.builder()
                    .number(createUpdateRaceDTO.getNumber())
                    .redHelmetHostsPlayer(redHelmetHostsPlayer)
                    .pointsRedHelmetHostsPlayer(createUpdateRaceDTO.getPointsRedHelmetHostsPlayer())
                    .blueHelmetHostsPlayer(blueHelmetHostsPlayer)
                    .pointsBlueHelmetHostsPlayer(createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer())
                    .whiteHelmetGuestsPlayer(whiteHelmetGuestPlayer)
                    .pointsWhiteHelmetGuestsPlayer(createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer())
                    .yellowHelmetGuestsPlayer(yellowHelmetGuestsPlayer)
                    .pointsYellowHelmetGuestsPlayer(createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer())
                    .game(game)
                    .build();


            game.setHostPoints(game.getHostPoints() + race.getPointsRedHelmetHostsPlayer() + race.getPointsBlueHelmetHostsPlayer());
            game.setGuestPoints(game.getGuestPoints() + race.getPointsWhiteHelmetGuestsPlayer() + race.getPointsYellowHelmetGuestsPlayer());
            Race savedRace = raceRepository.save(race);
            game.getRaces().add(savedRace);
            redHelmetHostsPlayer.getRedHelmetHostsPlayerRaces().add(savedRace);
            blueHelmetHostsPlayer.getBlueHelmetHostsPlayerRaces().add(savedRace);
            whiteHelmetGuestPlayer.getWhiteHelmetGuestsPlayerRaces().add(savedRace);
            yellowHelmetGuestsPlayer.getYellowHelmetGuestsPlayerRaces().add(savedRace);

            return raceDTOMapper.toDto(savedRace);
        }
        throw new AlreadyExistsException("A race with this number is already exists!");
    }


    @Transactional
    public RaceDTO updateRace(Long id, CreateUpdateRaceDTO createUpdateRaceDTO) throws NotFoundException, InvalidData, AlreadyExistsException {
        validate(createUpdateRaceDTO);
        Race race = raceRepository.findById(id).orElseThrow(() -> new NotFoundException("Race not found!"));
        Game game = race.getGame();

        Player redHelmetHostsPlayer = playerRepository.findById(createUpdateRaceDTO.getRedHelmetHostsPlayerId()).orElseThrow(() -> new NotFoundException("Red player hosts not found!"));
        Player blueHelmetHostsPlayer = playerRepository.findById(createUpdateRaceDTO.getBlueHelmetHostsPlayerId()).orElseThrow(() -> new NotFoundException("Blue helmet hosts player not found!"));
        Player whiteHelmetGuestPlayer = playerRepository.findById(createUpdateRaceDTO.getWhiteHelmetGuestsPlayerId()).orElseThrow(() -> new NotFoundException("White helmet guests player not found!"));
        Player yellowHelmetGuestsPlayer = playerRepository.findById(createUpdateRaceDTO.getYellowHelmetGuestsPlayerId()).orElseThrow(() -> new NotFoundException("Yellow helmet guests player not found!"));
        if (redHelmetHostsPlayer.equals(blueHelmetHostsPlayer) ||
                whiteHelmetGuestPlayer.equals(yellowHelmetGuestsPlayer) ||
                createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() ||
                createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() == createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() ||
                createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer()) {
            throw new InvalidData("Players of the same team and points of all players in the race must be different!");
        }

        boolean existsRace = false;
        if (game.getRaces() != null) {
            for (Race gameRace : game.getRaces()) {
                if (gameRace.getNumber() == createUpdateRaceDTO.getNumber()) {
                    if (gameRace.getRedHelmetHostsPlayer().getId().equals(createUpdateRaceDTO.getRedHelmetHostsPlayerId()) &&
                            gameRace.getPointsRedHelmetHostsPlayer() == createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() &&
                            gameRace.getBlueHelmetHostsPlayer().getId().equals(createUpdateRaceDTO.getBlueHelmetHostsPlayerId()) &&
                            gameRace.getPointsBlueHelmetHostsPlayer() == createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() &&
                            gameRace.getWhiteHelmetGuestsPlayer().getId().equals(createUpdateRaceDTO.getWhiteHelmetGuestsPlayerId()) &&
                            gameRace.getPointsWhiteHelmetGuestsPlayer() == createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() &&
                            gameRace.getYellowHelmetGuestsPlayer().getId().equals(createUpdateRaceDTO.getYellowHelmetGuestsPlayerId()) &&
                            gameRace.getPointsYellowHelmetGuestsPlayer() == createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() ||
                            race.getNumber() != createUpdateRaceDTO.getNumber()) {
                        existsRace = true;
                    }
                }
            }
        }


        if (!existsRace) {
            game.setHostPoints(game.getHostPoints() - race.getPointsRedHelmetHostsPlayer() - race.getPointsBlueHelmetHostsPlayer());
            game.setGuestPoints(game.getGuestPoints() - race.getPointsWhiteHelmetGuestsPlayer() - race.getPointsYellowHelmetGuestsPlayer());
            race.setNumber(createUpdateRaceDTO.getNumber());
            race.setRedHelmetHostsPlayer(redHelmetHostsPlayer);
            race.setPointsRedHelmetHostsPlayer(createUpdateRaceDTO.getPointsRedHelmetHostsPlayer());
            race.setBlueHelmetHostsPlayer(blueHelmetHostsPlayer);
            race.setPointsBlueHelmetHostsPlayer(createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer());
            race.setWhiteHelmetGuestsPlayer(whiteHelmetGuestPlayer);
            race.setPointsWhiteHelmetGuestsPlayer(createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer());
            race.setYellowHelmetGuestsPlayer(yellowHelmetGuestsPlayer);
            race.setPointsYellowHelmetGuestsPlayer(createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer());

            game.setHostPoints(game.getHostPoints() + createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() + createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer());
            game.setGuestPoints(game.getGuestPoints() + createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() + createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer());

            Race savedRace = raceRepository.save(race);

            return raceDTOMapper.toDto(savedRace);
        }
        throw new AlreadyExistsException("Race with this data is already exists!");
    }


    @Transactional
    public RaceDTO deleteById(Long id) throws NotFoundException {
        Race existsRace = raceRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this race!"));
        Game game = existsRace.getGame();
        Player redHelmetPlayer = existsRace.getRedHelmetHostsPlayer();
        redHelmetPlayer.getRedHelmetHostsPlayerRaces().remove(existsRace);
        Player blueHelmetPlayer = existsRace.getBlueHelmetHostsPlayer();
        blueHelmetPlayer.getBlueHelmetHostsPlayerRaces().remove(existsRace);
        Player whiteHelmetPlayer = existsRace.getWhiteHelmetGuestsPlayer();
        whiteHelmetPlayer.getWhiteHelmetGuestsPlayerRaces().remove(existsRace);
        Player yellowHelmetPlayer = existsRace.getYellowHelmetGuestsPlayer();
        yellowHelmetPlayer.getYellowHelmetGuestsPlayerRaces().remove(existsRace);
        game.setHostPoints(game.getHostPoints() - existsRace.getPointsRedHelmetHostsPlayer() - existsRace.getPointsBlueHelmetHostsPlayer());
        game.setGuestPoints(game.getGuestPoints() - existsRace.getPointsWhiteHelmetGuestsPlayer() - existsRace.getPointsYellowHelmetGuestsPlayer());
        raceRepository.delete(existsRace);
        return raceDTOMapper.toDto(existsRace);
    }

    private void validate(CreateUpdateRaceDTO createUpdateRaceDTO) throws InvalidData {
        if (createUpdateRaceDTO.getNumber() < 1 || createUpdateRaceDTO.getNumber() > 15) {
            throw new InvalidData("Race number must be higher then or equal to 1 and less then or equal to 15");
        }
        if (createUpdateRaceDTO.getRedHelmetHostsPlayerId() <= 0) {
            throw new InvalidData("Race must have an non-empty red helmet hosts player data!");
        }
        if (createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() < 0 || createUpdateRaceDTO.getPointsRedHelmetHostsPlayer() > 3) {
            throw new InvalidData("Race red helmet hosts player points must be higher then or equal to 0 and less then or equal to 3");
        }
        if (createUpdateRaceDTO.getBlueHelmetHostsPlayerId() <= 0) {
            throw new InvalidData("Race must have an non-empty blue helmet hosts player data!");
        }
        if (createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() < 0 || createUpdateRaceDTO.getPointsBlueHelmetHostsPlayer() > 3) {
            throw new InvalidData("Race blue helmet hosts player points must be higher then or equal to 0 and less then or equal to 3");
        }
        if (createUpdateRaceDTO.getWhiteHelmetGuestsPlayerId() <= 0) {
            throw new InvalidData("Race must have an non-empty white helmet guests player data!");
        }
        if (createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() < 0 || createUpdateRaceDTO.getPointsWhiteHelmetGuestsPlayer() > 3) {
            throw new InvalidData("Race white helmet guests player points must be higher then or equal to 0 and less then or equal to 3");
        }
        if (createUpdateRaceDTO.getYellowHelmetGuestsPlayerId() <= 0) {
            throw new InvalidData("Race must have an non-empty yellow helmet guests player data!");
        }
        if (createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() < 0 || createUpdateRaceDTO.getPointsYellowHelmetGuestsPlayer() > 3) {
            throw new InvalidData("Race yellow helmet guests player points must be higher then or equal to 0 and less then or equal to 3");
        }
    }
}


