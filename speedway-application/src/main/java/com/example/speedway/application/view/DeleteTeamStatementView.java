package com.example.speedway.application.view;

import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DeleteTeamStatementView {
    @Autowired
    private TeamRepository teamRepository;

    @GetMapping("/delete_team_statement")
    public ModelAndView deleteTeamStatementView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("delete_team_statement");
        Team team = teamRepository.findById(id).orElseThrow(() -> new NotFoundException("Team not found!"));
        modelAndView.addObject("team", team);

        User user = team.getUser();
        modelAndView.addObject("user", user);
        return modelAndView;
    }
}
