package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUserDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CreateUserView {
    @Autowired
    private UserService userService;

    @GetMapping("/create_user")
    public ModelAndView displayCreateUserForm() {
        ModelAndView modelAndView = new ModelAndView("create_user");
        CreateUserDTO createUserDTO = new CreateUserDTO();
        modelAndView.addObject("createUserDTO", createUserDTO);
        return modelAndView;
    }

    @PostMapping("/create_user")
    public String createUser(@Valid @ModelAttribute CreateUserDTO createUserDTO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUserDTO", createUserDTO);
            return "create_user";
        }
        try {
            userService.createUser(createUserDTO);
        } catch (AlreadyExistsException alreadyExistsException) {
            return "redirect:/user_already_exists_error";
        } catch (InvalidData invalidData) {
            return "redirect:/different_passwords_error";
        }
        return "redirect:/welcome_in_application/" + createUserDTO.getLogin();
    }
}
