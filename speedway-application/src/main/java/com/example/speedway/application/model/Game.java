package com.example.speedway.application.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String description;
    private LocalDate date;
    @ManyToOne
    private Team hosts;
    private int hostPoints;
    @ManyToOne
    private Team guests;
    private int guestPoints;
    @OneToMany(mappedBy = "game")
    private List<Race> races = new ArrayList<>();
    @ManyToOne
    private User user;
}
