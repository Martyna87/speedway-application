package com.example.speedway.application.exceptions;

public class InvalidData extends Exception {
    public InvalidData(String message) {
        super(message);
    }
}
