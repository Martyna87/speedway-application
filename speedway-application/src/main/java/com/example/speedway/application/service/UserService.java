package com.example.speedway.application.service;

import com.example.speedway.application.DTO.CreateUserDTO;
import com.example.speedway.application.DTO.UpdateUserDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;
    @Autowired
    private GameService gameService;
    @Autowired
    private TeamService teamService;


    @Transactional
    public List<UserDTO> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(u -> userDTOMapper.toDto(u))
                .collect(Collectors.toList());
    }

    @Transactional
    public UserDTO getUserById(Long id) throws NotFoundException {
        return userRepository.findById(id)
                .map(u -> userDTOMapper.toDto(u))
                .orElseThrow(() -> new NotFoundException("Not found this user!"));
    }

    @Transactional
    public UserDTO getUserByLogin(String login) throws NotFoundException {
        return userRepository.findByLogin(login)
                .map(u -> userDTOMapper.toDto(u))
                .orElseThrow(() -> new NotFoundException("Not found this user!"));
    }

    @Transactional
    public UserDTO createUser(CreateUserDTO createUserDTO) throws InvalidData, AlreadyExistsException {
        validate(createUserDTO);
        if (!createUserDTO.getPassword().equals(createUserDTO.getRePassword())) {
            throw new InvalidData("Password and repeated password must be the same!");
        }
        boolean exist = userRepository.existsByLogin(createUserDTO.getLogin());
        if (!exist) {
            User user = User.builder()
                    .login(createUserDTO.getLogin())
                    .password(createUserDTO.getPassword())
                    .firstName(createUserDTO.getFirstName())
                    .lastName(createUserDTO.getLastName())
                    .email(createUserDTO.getEmail())
                    .city(createUserDTO.getCity())
                    .build();

            User savedUser = userRepository.save(user);
            return userDTOMapper.toDto(savedUser);
        }
        throw new AlreadyExistsException("User with this data is already exists!");
    }


    @Transactional
    public UserDTO updateUser(UpdateUserDTO updateUserDTO, Long id) throws NotFoundException, InvalidData {
        validate(updateUserDTO);
        if (!updateUserDTO.getPassword().equals(updateUserDTO.getRePassword())) {
            throw new InvalidData("Password and repeated password must be the same!");
        }
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this user!"));
        user.setPassword(updateUserDTO.getPassword());
        user.setFirstName(updateUserDTO.getFirstName());
        user.setLastName(updateUserDTO.getLastName());
        user.setEmail(updateUserDTO.getEmail());
        user.setCity(updateUserDTO.getCity());

        User savedUser = userRepository.save(user);
        return userDTOMapper.toDto(savedUser);
    }

    @Transactional
    public UserDTO deletedById(Long id) throws NotFoundException {
        User existingUser = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this user!"));
        List<Game> userGame = existingUser.getUserGames();
        Iterator<Game> iteratorUserGames = userGame.iterator();
        while (iteratorUserGames.hasNext()) {
            Game game = iteratorUserGames.next();
            iteratorUserGames.remove();
            gameService.deleteById(game.getId());

        }
            
        List<Team> userTeam = existingUser.getUserTeams();
        Iterator<Team> iteratorUserTeams = userTeam.iterator();
        while (iteratorUserTeams.hasNext()) {
            Team team = iteratorUserTeams.next();
            iteratorUserTeams.remove();
            teamService.deletedById(team.getId());
        }
        userRepository.delete(existingUser);
        return userDTOMapper.toDto(existingUser);
    }


    private void validate(CreateUserDTO createUserDTO) throws InvalidData {
        if (createUserDTO.getLogin() == null || createUserDTO.getLogin().isEmpty()) {
            throw new InvalidData("User must have non-empty login!");
        }
        if (createUserDTO.getLogin().length() < 3) {
            throw new InvalidData("Login must have a minimum of 3 characters!");
        }
        if (createUserDTO.getPassword() == null || createUserDTO.getPassword().isEmpty()) {
            throw new InvalidData("User must have non-empty password!");
        }
        if (createUserDTO.getPassword().length() < 8) {
            throw new InvalidData("Password must have a minimum of 8 characters!");
        }
        if (createUserDTO.getRePassword() == null || createUserDTO.getRePassword().isEmpty()) {
            throw new InvalidData("User must have non-empty repeated password!");
        }
        if (createUserDTO.getRePassword().length() < 8) {
            throw new InvalidData("Repeated password must have a minimum of 8 characters!");
        }
        if (createUserDTO.getFirstName() == null || createUserDTO.getFirstName().isEmpty()) {
            throw new InvalidData("User must have non-empty first name!");
        }
        if (createUserDTO.getFirstName().length() < 3) {
            throw new InvalidData("First name must have a minimum of 3 characters!");
        }
        if (createUserDTO.getLastName() == null || createUserDTO.getLastName().isEmpty()) {
            throw new InvalidData("User must have non-empty last name!");
        }
        if (createUserDTO.getLastName().length() < 3) {
            throw new InvalidData("Last name must have a minimum of 3 characters!");
        }
        if (createUserDTO.getEmail() == null || createUserDTO.getEmail().isEmpty()) {
            throw new InvalidData("User must have non-empty email!");
        }
        if (createUserDTO.getCity() == null || createUserDTO.getCity().isEmpty()) {
            throw new InvalidData("User must have non-empty city!");
        }
        if (createUserDTO.getCity().length() < 3) {
            throw new InvalidData("City must have a minimum of 3 characters!");
        }
    }

    private void validate(UpdateUserDTO updateUserDTO) throws InvalidData {
        if (updateUserDTO.getPassword() == null || updateUserDTO.getPassword().isEmpty()) {
            throw new InvalidData("User must have non-empty password!");
        }
        if (updateUserDTO.getPassword().length() < 8) {
            throw new InvalidData("Password must have a minimum of 8 characters!");
        }
        if (updateUserDTO.getRePassword() == null || updateUserDTO.getRePassword().isEmpty()) {
            throw new InvalidData("User must have non-empty repeated password!");
        }
        if (updateUserDTO.getRePassword().length() < 8) {
            throw new InvalidData("Repeated password must have a minimum of 8 characters!");
        }
        if (updateUserDTO.getFirstName() == null || updateUserDTO.getFirstName().isEmpty()) {
            throw new InvalidData("User must have non-empty first name!");
        }
        if (updateUserDTO.getFirstName().length() < 3) {
            throw new InvalidData("First name must have a minimum of 3 characters!");
        }
        if (updateUserDTO.getLastName() == null || updateUserDTO.getLastName().isEmpty()) {
            throw new InvalidData("User must have non-empty last name!");
        }
        if (updateUserDTO.getLastName().length() < 3) {
            throw new InvalidData("Last name must have a minimum of 3 characters!");
        }
        if (updateUserDTO.getEmail() == null || updateUserDTO.getEmail().isEmpty()) {
            throw new InvalidData("User must have non-empty email!");
        }
        if (updateUserDTO.getCity() == null || updateUserDTO.getCity().isEmpty()) {
            throw new InvalidData("User must have non-empty city!");
        }
        if (updateUserDTO.getCity().length() < 3) {
            throw new InvalidData("City must have a minimum of 3 characters!");
        }
    }
}
