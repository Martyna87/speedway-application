package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.mapper.TeamDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TeamDetailsView {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TeamDTOMapper teamDTOMapper;
    @Autowired
    private GameDTOMapper gameDTOMapper;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;


    @GetMapping("team_details")
    public ModelAndView teamView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("team_details");
        Team team = teamRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found team!"));
        TeamDTO teamDTO = teamDTOMapper.toDto(team);
        modelAndView.addObject("teamDto", teamDTO);

        User user = team.getUser();
        List<PlayerDTO> userPlayersInTeam = new ArrayList<>();
        if (user.getUserTeams() != null) {
            for (Team userTeam : user.getUserTeams()) {
                if (userTeam.getTeamPlayers() != null) {
                    for (Player player : userTeam.getTeamPlayers()) {
                        if (player.getTeam().getName().equalsIgnoreCase(team.getName()))
                            userPlayersInTeam.add(playerDTOMapper.toDto(player));
                    }
                }
            }
        }
        userPlayersInTeam.sort(Comparator.comparing(PlayerDTO::getLastName));
        modelAndView.addObject("userPlayersInTeam", userPlayersInTeam);

        List<Game> allTeamGames = new ArrayList<>();
        allTeamGames.addAll(team.getGamesHosts());
        allTeamGames.addAll(team.getGamesGuests());
        List<GameDTO> allTeamGamesDTO = allTeamGames
                .stream()
                .map(g -> gameDTOMapper.toDto(g))
                .sorted(Comparator.comparing(GameDTO::getDate))
                .collect(Collectors.toList());
        modelAndView.addObject("allTeamGamesDTO", allTeamGamesDTO);

        Integer numberOfAllGames = allTeamGames.size();
        modelAndView.addObject("numberOfAllGames", numberOfAllGames);

        List<Game> winGames = new ArrayList<>();
        List<Game> tiedGames = new ArrayList<>();
        for (Game game : allTeamGames) {
            if ((game.getHosts().getName().equalsIgnoreCase(team.getName()) && game.getHostPoints() > game.getGuestPoints()) || (game.getGuests().getName().equalsIgnoreCase(team.getName()) && game.getGuestPoints() > game.getHostPoints())) {
                winGames.add(game);
            } else if ((game.getHosts().getName().equalsIgnoreCase(team.getName()) && game.getHostPoints() == game.getGuestPoints()) || (game.getGuests().getName().equalsIgnoreCase(team.getName()) && game.getGuestPoints() == game.getHostPoints())) {
                tiedGames.add(game);
            }
        }
        Integer numberGamesWon = winGames.size();
        modelAndView.addObject("numberGamesWon", numberGamesWon);

        Integer numberGamesTied = tiedGames.size();
        modelAndView.addObject("numberGamesTied", numberGamesTied);

        Integer numberGamesLost = allTeamGames.size() - winGames.size() - tiedGames.size();
        modelAndView.addObject("numberGamesLost", numberGamesLost);

        Integer sumPointsScored = 0;
        Integer sumPointsLost = 0;
        for (Game game : allTeamGames) {
            if (game.getHosts().getName().equalsIgnoreCase(team.getName())) {
                sumPointsScored += game.getHostPoints();
                sumPointsLost += game.getGuestPoints();
            } else {
                sumPointsScored += game.getGuestPoints();
                sumPointsLost += game.getHostPoints();
            }
        }
        modelAndView.addObject("sumPointsScored", sumPointsScored);
        modelAndView.addObject("sumPointsLost", sumPointsLost);

        java.text.DecimalFormat df = new java.text.DecimalFormat();
        df.setMaximumFractionDigits(2);
        Double averageOfPointsScored = (double) sumPointsScored / (double) numberOfAllGames;
        Double averageOfPointsLost = (double) sumPointsLost / (double) numberOfAllGames;
        modelAndView.addObject("averageOfPointsScored", df.format(averageOfPointsScored));
        modelAndView.addObject("averageOfPointsLost", df.format(averageOfPointsLost));
        return modelAndView;
    }
}
