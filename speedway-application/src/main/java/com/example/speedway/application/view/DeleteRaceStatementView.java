package com.example.speedway.application.view;

import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.repository.RaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DeleteRaceStatementView {
    @Autowired
    private RaceRepository raceRepository;

    @GetMapping("/delete_race_statement")
    public ModelAndView deleteRaceStatementView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("delete_race_statement");
        Race race = raceRepository.findById(id).orElseThrow(() -> new NotFoundException("Race not found!"));
        modelAndView.addObject("race", race);

        Game game = race.getGame();
        modelAndView.addObject("game", game);
        return modelAndView;
    }
}
