package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateUserDTO {
    @NotBlank(message = "Password must be filled")
    @Size(min = 8, message = "Password must be at least 8 chars long")
    private String password;
    @NotBlank(message = "Repeated password must be filled")
    @Size(min = 8, message = "Repeated password must be at least 8 chars long")
    private String rePassword;
    @NotBlank(message = "First name must be filled")
    @Size(min = 3, message = "First name must be at least 3 chars long")
    private String firstName;
    @NotBlank(message = "Last name must be filled")
    @Size(min = 3, message = "Last name must be at least 3 chars long")
    private String lastName;
    @NotBlank(message = "Email must be filled")
    @Email(message = "Email must be valid")
    private String email;
    @NotBlank(message = "City must be filled")
    @Size(min = 3, message = "City must be at least 3 chars long")
    private String city;
}

