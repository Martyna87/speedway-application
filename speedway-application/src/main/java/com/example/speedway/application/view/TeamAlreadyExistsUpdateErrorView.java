package com.example.speedway.application.view;

import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.TeamDTOMapper;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TeamAlreadyExistsUpdateErrorView {
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    TeamDTOMapper teamDTOMapper;
    @Autowired
    UserDTOMapper userDTOMapper;

    @GetMapping("/team_already_exists_update_error/{id}")
    public ModelAndView errorUpdateTeamView(@PathVariable Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("team_already_exists_update_error");
        Team team = teamRepository.findById(id).orElseThrow(() -> new NotFoundException("Team not found!"));
        TeamDTO teamDTO = teamDTOMapper.toDto(team);
        modelAndView.addObject("teamDTO", teamDTO);

        User user = team.getUser();
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO", userDTO);
        return modelAndView;
    }
}
