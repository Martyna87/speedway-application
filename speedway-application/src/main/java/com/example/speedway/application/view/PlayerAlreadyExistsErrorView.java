package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PlayerAlreadyExistsErrorView {
    @GetMapping("/player_already_exists_error")
    public ModelAndView errorCreatePlayerView() {
        ModelAndView modelAndView = new ModelAndView("player_already_exists_error");
        return modelAndView;
    }
}
