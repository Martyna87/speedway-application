package com.example.speedway.application.view;

import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomepageView {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;

    @GetMapping("/homepage")
    public ModelAndView homepageView(Authentication authentication) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("homepage");
        String login = null;
        if (authentication != null) {
            login = authentication.getName();
        }
        modelAndView.addObject("login", login);

        UserDTO userDTO = null;
        if (login != null) {
            User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User not found!"));
            userDTO = userDTOMapper.toDto(user);
        }
        modelAndView.addObject("userDTO", userDTO);
        return modelAndView;
    }
}
