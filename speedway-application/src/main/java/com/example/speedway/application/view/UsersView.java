package com.example.speedway.application.view;

import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class UsersView {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ModelAndView getAllUsersDto() {
        ModelAndView modelAndView = new ModelAndView("users_dto");
        List<UserDTO> usersDto = userService.getAllUsers();
        modelAndView.addObject("usersDto", usersDto);
        return modelAndView;
    }

    @GetMapping("/delete_user")
    public String deleteUser(@RequestParam Long id) throws NotFoundException {
        userService.deletedById(id);
        return "redirect:/logout";
    }
}