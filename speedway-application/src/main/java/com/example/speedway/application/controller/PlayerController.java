package com.example.speedway.application.controller;

import com.example.speedway.application.DTO.CreatePlayerDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.UpdatePlayerDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/player")
public class PlayerController {
    @Autowired
    private PlayerService playerService;

    @GetMapping
    public List<PlayerDTO> getAllPlayers() {
        return playerService.getAllPlayers();
    }

    @GetMapping("/{id}")
    public PlayerDTO getById(@PathVariable Long id) throws NotFoundException {
        return playerService.getPlayerById(id);
    }

    @PostMapping
    public PlayerDTO createPlayer(@Valid @RequestBody CreatePlayerDTO createPlayerDTO, String login) throws AlreadyExistsException, InvalidData, NotFoundException {
        return playerService.createPlayer(createPlayerDTO, login);
    }

    @PutMapping("/{id}")
    public PlayerDTO updatePlayer(@Valid @RequestBody UpdatePlayerDTO updatePlayerDTO, @PathVariable Long id) throws InvalidData, AlreadyExistsException, NotFoundException {
        return playerService.updatePlayer(id, updatePlayerDTO);
    }

    @DeleteMapping("/{id}")
    public PlayerDTO deletePlayer(@PathVariable Long id) throws NotFoundException {
        return playerService.deletedById(id);
    }
}
