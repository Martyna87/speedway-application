package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateUpdateRaceDTO {
    @NotNull(message = "Race number must be selected")
    @Min(value = 1, message = "Race number must be greater than or equal to 1")
    @Max(value = 15, message = "Race number must be less than or equal to 15")
    private int number;
    @NotNull(message = "Red helmet hosts player must be selected")
    private Long redHelmetHostsPlayerId;
    @NotNull(message = "Red helmet hosts player points must be selected")
    @Min(value = 0, message = "Red helmet hosts player points must be greater than or equal to 0")
    @Max(value = 3, message = "Red helmet hosts player points must be less than or equal to 3")
    private int pointsRedHelmetHostsPlayer;
    @NotNull(message = "Blue helmet hosts player must be selected")
    private Long blueHelmetHostsPlayerId;
    @NotNull(message = "Blue helmet hosts player points must be selected")
    @Min(value = 0, message = "Blue helmet hosts player points must be greater than or equal to 0")
    @Max(value = 3, message = "Blue helmet hosts player points number must be less than or equal to 3")
    private int pointsBlueHelmetHostsPlayer;
    @NotNull(message = "White helmet guests player must be selected")
    private Long whiteHelmetGuestsPlayerId;
    @NotNull(message = "White helmet guests player points must be selected")
    @Min(value = 0, message = "White helmet guests player points must be greater than or equal to 0")
    @Max(value = 3, message = "White helmet guests player points must be less than or equal to 3")
    private int pointsWhiteHelmetGuestsPlayer;
    @NotNull(message = "Yellow helmet guests player must be selected")
    private Long yellowHelmetGuestsPlayerId;
    @NotNull(message = "Yellow helmet guests player points must be selected")
    @Min(value = 0, message = "Yellow helmet guests player points must be greater than or equal to 0")
    @Max(value = 3, message = "Yellow helmet guests player points must be less than or equal to 3")
    private int pointsYellowHelmetGuestsPlayer;


}
