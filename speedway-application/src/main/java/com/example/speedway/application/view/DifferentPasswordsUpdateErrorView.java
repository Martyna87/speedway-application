package com.example.speedway.application.view;

import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DifferentPasswordsUpdateErrorView {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;

    @GetMapping("/different_passwords_update_error/{id}")
    public ModelAndView differentPasswordsErrorView(@PathVariable Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("different_passwords_update_error");
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found!"));
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO", userDTO);
        return modelAndView;
    }
}
