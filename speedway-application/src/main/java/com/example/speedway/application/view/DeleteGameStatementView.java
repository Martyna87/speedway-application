package com.example.speedway.application.view;

import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DeleteGameStatementView {
    @Autowired
    private GameRepository gameRepository;

    @GetMapping("/delete_game_statement")
    public ModelAndView deleteGameStatementView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("delete_game_statement");
        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Game not found!"));
        modelAndView.addObject("game", game);

        User user = game.getUser();
        modelAndView.addObject("user", user);
        return modelAndView;
    }
}
