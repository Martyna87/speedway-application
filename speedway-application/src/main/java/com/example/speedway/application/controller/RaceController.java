package com.example.speedway.application.controller;

import com.example.speedway.application.DTO.CreateUpdateRaceDTO;
import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/race")
public class RaceController {
    @Autowired
    private RaceService raceService;

    @GetMapping
    public List<RaceDTO> getAllRaces() {
        return raceService.getAllRaces();
    }

    @GetMapping("/{id}")
    public RaceDTO getById(@PathVariable Long id) throws NotFoundException {
        return raceService.getRaceById(id);
    }

    @PostMapping
    public RaceDTO createRace(@Valid @RequestBody CreateUpdateRaceDTO createUpdateRaceDTO, Long gameId) throws InvalidData, AlreadyExistsException, NotFoundException {
        return raceService.createRace(createUpdateRaceDTO, gameId);
    }

    @PutMapping("/{id}")
    public RaceDTO updateRace(@Valid @RequestBody CreateUpdateRaceDTO createUpdateRaceDTO, @PathVariable Long id) throws InvalidData, AlreadyExistsException, NotFoundException {
        return raceService.updateRace(id, createUpdateRaceDTO);
    }

    @DeleteMapping("/{id}")
    public RaceDTO deleteRace(@PathVariable Long id) throws NotFoundException {
        return raceService.deleteById(id);
    }
}
