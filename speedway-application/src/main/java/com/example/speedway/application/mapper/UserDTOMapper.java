package com.example.speedway.application.mapper;

import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserDTOMapper {
    public UserDTO toDto(User user) {
        List<Long> userTeamsIds = null;
        if (user.getUserTeams() != null) {
            userTeamsIds = user.getUserTeams().stream()
                    .map(u -> u.getId())
                    .collect(Collectors.toList());
        }

        List<Long> userGamesIds = null;
        if (user.getUserGames() != null) {
            userGamesIds = user.getUserGames().stream()
                    .map(u -> u.getId())
                    .collect(Collectors.toList());
        }

        return UserDTO.builder()
                .id(user.getId())
                .login(user.getLogin())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .city(user.getCity())
                .isAdmin(user.isAdmin())
                .userGamesIds(userGamesIds)
                .userTeamsIds(userTeamsIds)
                .build();
    }
}
