package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUpdateRaceDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class CreateRaceView {
    @Autowired
    private RaceService raceService;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;


    @GetMapping("/create_races")
    public ModelAndView displayCreateGameForm(@RequestParam Long gameId) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("create_race");
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new NotFoundException("Game not found!"));
        modelAndView.addObject("gameId", gameId);

        CreateUpdateRaceDTO createUpdateRaceDTO = new CreateUpdateRaceDTO();
        modelAndView.addObject("createUpdateRaceDTO", createUpdateRaceDTO);

        List<PlayerDTO> hostsPlayers = new ArrayList<>();
        if (game.getHosts().getTeamPlayers() != null) {
            hostsPlayers = game.getHosts()
                    .getTeamPlayers()
                    .stream()
                    .map(p -> playerDTOMapper.toDto(p))
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("hostsPlayers", hostsPlayers);

        List<PlayerDTO> guestsPlayers = new ArrayList<>();
        if (game.getGuests().getTeamPlayers() != null) {
            guestsPlayers = game.getGuests()
                    .getTeamPlayers()
                    .stream()
                    .map(p -> playerDTOMapper.toDto(p))
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("guestsPlayers", guestsPlayers);

        List<Integer> points = new ArrayList<>();
        for (int point = 0; point < 4; point++) {
            points.add(point);
        }
        modelAndView.addObject("points", points);

        List<Integer> raceNumbers = new ArrayList<>();
        for (int raceNumber = 1; raceNumber < 16; raceNumber++) {
            raceNumbers.add(raceNumber);
        }
        modelAndView.addObject("raceNumbers", raceNumbers);

        return modelAndView;
    }


    @PostMapping("/create_races/{gameId}")
    public String createRaces(@Valid @ModelAttribute CreateUpdateRaceDTO createUpdateRaceDTO, @PathVariable Long gameId, BindingResult bindingResult, Model model) throws NotFoundException {
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new NotFoundException("Game not found!"));
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUpdateRaceDTO", createUpdateRaceDTO);

            List<PlayerDTO> hostsPlayers = new ArrayList<>();
            if (game.getHosts().getTeamPlayers() != null) {
                hostsPlayers = game.getHosts()
                        .getTeamPlayers()
                        .stream()
                        .map(p -> playerDTOMapper.toDto(p))
                        .collect(Collectors.toList());
            }
            model.addAttribute("hostsPlayers", hostsPlayers);

            List<PlayerDTO> guestsPlayers = new ArrayList<>();
            if (game.getGuests().getTeamPlayers() != null) {
                guestsPlayers = game.getGuests()
                        .getTeamPlayers()
                        .stream()
                        .map(p -> playerDTOMapper.toDto(p))
                        .collect(Collectors.toList());
            }
            model.addAttribute("guestsPlayers", guestsPlayers);

            List<Integer> points = new ArrayList<>();
            for (int point = 0; point < 4; point++) {
                points.add(point);
            }
            model.addAttribute("points", points);

            List<Integer> raceNumbers = new ArrayList<>();
            for (int raceNumber = 1; raceNumber < 16; raceNumber++) {
                raceNumbers.add(raceNumber);
            }
            model.addAttribute("raceNumbers", raceNumbers);
            return "create_race";
        }
        try {
            raceService.createRace(createUpdateRaceDTO, gameId);
        } catch (InvalidData invalidData) {
            return "redirect:/the_same_player_or_points_error/" + gameId;
        } catch (AlreadyExistsException alreadyExistsException) {
            return "redirect:/race_already_exists_error/" + gameId;
        } catch (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }
        return "redirect:/game_details?id=" + gameId;

    }
}
