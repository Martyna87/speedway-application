package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DifferentPasswordsErrorView {
    @GetMapping("/different_passwords_error")
    public ModelAndView differentPasswordsErrorView() {
        ModelAndView modelAndView = new ModelAndView("different_passwords_error");
        return modelAndView;
    }
}
