package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GameDTO {
    private Long id;
    private String description;
    private LocalDate date;
    private String hostsName;
    private int hostPoints;
    private String guestsName;
    private int guestPoints;
    private List<Long> raceIds = new ArrayList<>();
    private String userLogin;
}
