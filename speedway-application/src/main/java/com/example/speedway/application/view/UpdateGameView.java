package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUpdateGameDTO;
import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UpdateGameView {
    @Autowired
    private GameService gameService;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameDTOMapper gameDTOMapper;

    @GetMapping("/update_game")
    public ModelAndView displayUpdateGameView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("update_game_form");
        modelAndView.addObject("id", id);

        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this game!"));
        GameDTO gameDTO = gameDTOMapper.toDto(game);
        CreateUpdateGameDTO createUpdateGameDTO = new CreateUpdateGameDTO();
        createUpdateGameDTO.setDescription(gameDTO.getDescription());
        createUpdateGameDTO.setDate(gameDTO.getDate());
        createUpdateGameDTO.setHostsName(gameDTO.getHostsName());
        createUpdateGameDTO.setGuestsName(gameDTO.getGuestsName());
        modelAndView.addObject("createUpdateGameDTO", createUpdateGameDTO);

        User user = game.getUser();
        List<String> userTeamNames = null;
        if (user.getUserTeams() != null) {
            userTeamNames = user.getUserTeams()
                    .stream()
                    .map(t -> t.getName())
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("userTeamNames", userTeamNames);
        return modelAndView;

    }

    @PostMapping("/update_game/{id}")
    public String updateGame(@PathVariable Long id, @Valid @ModelAttribute CreateUpdateGameDTO createUpdateGameDTO, BindingResult bindingResult, Model model) throws NotFoundException {
        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException(("Game not found!")));
        User user = game.getUser();
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUpdateGameDTO", createUpdateGameDTO);
            List<String> userTeamNames = null;
            if (user.getUserTeams() != null) {
                userTeamNames = user.getUserTeams()
                        .stream()
                        .map(t -> t.getName())
                        .collect(Collectors.toList());
            }
            model.addAttribute("userTeamNames", userTeamNames);
            return "update_game_form";
        }
        try {
            gameService.updateGame(id, createUpdateGameDTO);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/game_already_exists_update_error/" + game.getId();
        } catch (InvalidData invalidData) {
            return "redirect:/host_the_same_as_guest_update_error/" + game.getId();
        } catch (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }
        return "redirect:/game_details?id={id}";
    }


}
