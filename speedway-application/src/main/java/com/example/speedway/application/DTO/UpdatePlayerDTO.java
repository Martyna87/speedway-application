package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatePlayerDTO {
    @NotBlank(message = "First name must be filled")
    @Size(min = 3, message = "First name must be at least 3 chars long")
    private String firstName;
    @NotBlank(message = "Last name must be filled")
    @Size(min = 3, message = "Last name must be at least 3 chars long")
    private String lastName;
    private String nickName;
    @Min(value = 1901, message = "Birth year must be higher than 1900")
    private int birthYear;
    @NotBlank(message = "Nationality must be selected")
    private String nationality;

}
