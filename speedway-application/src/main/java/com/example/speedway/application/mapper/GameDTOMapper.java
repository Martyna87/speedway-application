package com.example.speedway.application.mapper;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.model.Game;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GameDTOMapper {
    public GameDTO toDto(Game game) {
        List<Long> raceIds = null;
        if (game.getRaces() != null) {
            raceIds = game.getRaces().stream()
                    .map(g -> g.getId())
                    .collect(Collectors.toList());
        }

        String userLogin = null;
        if (game.getUser() != null) {
            userLogin = game.getUser()
                    .getLogin();
        }

        String hostsName = null;
        if (game.getHosts() != null) {
            hostsName = game.getHosts()
                    .getName();
        }

        String guestsName = null;
        if (game.getGuests() != null) {
            guestsName = game.getGuests()
                    .getName();
        }

        return GameDTO.builder()
                .id(game.getId())
                .description(game.getDescription())
                .date(game.getDate())
                .hostsName(hostsName)
                .hostPoints(game.getHostPoints())
                .guestsName(guestsName)
                .guestPoints(game.getGuestPoints())
                .raceIds(raceIds)
                .userLogin(userLogin)
                .build();

    }
}
