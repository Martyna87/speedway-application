package com.example.speedway.application.view;

import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.TeamRepository;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserTeamsView {
    @Autowired
    private TeamService teamService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;
    @Autowired
    private TeamRepository teamRepository;


    @GetMapping("/user_teams")
    public ModelAndView getAllUserTeamsDto (@RequestParam Long id) throws NotFoundException {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found!"));
        UserDTO userDTO = userDTOMapper.toDto(user);
        ModelAndView modelAndView = new ModelAndView("user_teams_dto");
        List<TeamDTO> teams = teamService.getAllTeams()
                .stream()
                .filter(t -> t.getUserLogin() == userDTO.getLogin())
                .collect(Collectors.toList());
        modelAndView.addObject("teams",teams);
        modelAndView.addObject("userDTO",userDTO);
        return modelAndView;
    }
    @GetMapping("/delete_team")
    public String teamDelete(@RequestParam Long id) throws NotFoundException {
        Team team = teamRepository.findById(id).orElseThrow(()-> new NotFoundException("Team not found!"));
        User user = team.getUser();
        teamService.deletedById(id);
        return "redirect:/user_teams?id=" + user.getId();

    }
}
