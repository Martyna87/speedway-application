package com.example.speedway.application.view;

import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.PlayerRepository;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserPlayersView {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;
    @Autowired
    private PlayerRepository playerRepository;


    @GetMapping("/user_players")
    public ModelAndView getAllUserPlayerDto(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("user_players_dto");
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found!"));
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO", userDTO);

        List<Team> userTeams = user.getUserTeams();
        modelAndView.addObject("userTeams", userTeams);

        List<Long> userTeamsIds = userDTO.getUserTeamsIds();
        List<PlayerDTO> players = playerService.getAllPlayers()
                .stream()
                .filter(p -> userTeamsIds.contains(p.getTeamId()))
                .sorted(Comparator.comparing(PlayerDTO::getTeamId))
                .collect(Collectors.toList());
        modelAndView.addObject("players", players);
        return modelAndView;
    }

    @GetMapping("/delete_player")
    public String playerDelete(@RequestParam Long id) throws NotFoundException {
        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Player not found!"));
        User user = player.getTeam().getUser();
        playerService.deletedById(id);
        return "redirect:/user_players?id=" + user.getId();
    }
}
