package com.example.speedway.application.mapper;

import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.model.Team;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamDTOMapper {
    public TeamDTO toDto(Team team) {
        List<Long> teamPlayersIds = null;
        if (team.getTeamPlayers() != null) {
            teamPlayersIds = team.getTeamPlayers().stream()
                    .map(t -> t.getId())
                    .collect(Collectors.toList());
        }
        List<Long> gameHostsIds = null;
        if (team.getGamesHosts() != null) {
            gameHostsIds = team.getGamesHosts().stream()
                    .map(t -> t.getId())
                    .collect(Collectors.toList());
        }
        List<Long> gameGuestsIds = null;
        if (team.getGamesGuests() != null) {
            gameGuestsIds = team.getGamesGuests().stream()
                    .map(t -> t.getId())
                    .collect(Collectors.toList());
        }

        String userLogin = null;
        if (team.getUser() != null) {
            userLogin = team.getUser().getLogin();
        }
        return TeamDTO.builder()
                .id(team.getId())
                .name(team.getName())
                .city(team.getCity())
                .coach(team.getCoach())
                .teamPlayersIds(teamPlayersIds)
                .gameHostsIds(gameHostsIds)
                .gameGuestsIds(gameGuestsIds)
                .userLogin(userLogin)
                .build();
    }
}
