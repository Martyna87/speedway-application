package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamDTO {
    private Long id;
    private String name;
    private String city;
    private String coach;
    private List<Long> teamPlayersIds = new ArrayList<>();
    private List<Long> gameHostsIds = new ArrayList<>();
    private List<Long> gameGuestsIds = new ArrayList<>();
    private String userLogin;
}
