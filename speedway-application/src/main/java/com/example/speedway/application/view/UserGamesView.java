package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.repository.RaceRepository;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.GameService;
import com.example.speedway.application.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserGamesView {
    @Autowired
    private GameService gameService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;
    @Autowired
    private GameRepository gameRepository;


    @GetMapping("/user_games")
    public ModelAndView getAllUserGamesDto(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("user_games_dto");
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found!"));
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO", userDTO);

        List<GameDTO> games = gameService.getAllGames()
                .stream()
                .filter(g -> g.getUserLogin() == userDTO.getLogin())
                .collect(Collectors.toList());
        modelAndView.addObject("games", games);
        return modelAndView;
    }

    @GetMapping("/delete_game")
    public String gameDelete(@RequestParam Long id) throws NotFoundException {
        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Game not found!"));
        User user = game.getUser();
        gameService.deleteById(id);
        return "redirect:/user_games?id=" + user.getId();
    }
}
