package com.example.speedway.application.view;

import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class UserProfileView {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;


    @GetMapping("/user_profile")
    public ModelAndView userProfileView(Authentication authentication) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("user_profile");
        String login = authentication.getName();
        User user  = userRepository.findByLogin(login).orElseThrow(()->new NotFoundException("Not found User!"));
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO",userDTO);
        return modelAndView;
    }
}
