package com.example.speedway.application.controller;

import com.example.speedway.application.DTO.CreateUserDTO;
import com.example.speedway.application.DTO.UpdateUserDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserDTO getById(@PathVariable Long id) throws NotFoundException {
        return userService.getUserById(id);
    }

    @GetMapping("/{login}")
    public UserDTO getByLogin(@PathVariable String login) throws NotFoundException {
        return userService.getUserByLogin(login);
    }

    @PostMapping
    public UserDTO createUser(@Valid @RequestBody CreateUserDTO createUpdateUserDTO) throws InvalidData, AlreadyExistsException {
        return userService.createUser(createUpdateUserDTO);
    }

    @PutMapping("/{id}")
    public UserDTO updateUser(@Valid @RequestBody UpdateUserDTO updateUserDTO, @PathVariable Long id) throws InvalidData, NotFoundException {
        return userService.updateUser(updateUserDTO, id);
    }

    @DeleteMapping("/{id}")
    public UserDTO deleteUser(@PathVariable Long id) throws NotFoundException {
        return userService.deletedById(id);
    }
}
