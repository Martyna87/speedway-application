package com.example.speedway.application.service;

import com.example.speedway.application.DTO.CreatePlayerDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.UpdatePlayerDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.model.*;
import com.example.speedway.application.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerService {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RaceService raceService;


    @Transactional
    public List<PlayerDTO> getAllPlayers() {
        return playerRepository.findAll()
                .stream()
                .map(p -> playerDTOMapper.toDto(p))
                .collect(Collectors.toList());
    }

    @Transactional
    public PlayerDTO getPlayerById(Long id) throws NotFoundException {
        return playerRepository.findById(id)
                .map(p -> playerDTOMapper.toDto(p))
                .orElseThrow(() -> new NotFoundException("Not found this player!"));
    }

    @Transactional
    public PlayerDTO createPlayer(CreatePlayerDTO createPlayerDTO, String login) throws AlreadyExistsException, InvalidData, NotFoundException {
        validate(createPlayerDTO);
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("Not found User!"));
        Team team = teamRepository.findById(createPlayerDTO.getTeamId()).orElseThrow(() -> new NotFoundException("Not found team by this name!"));
        boolean existPlayer = false;
        if (user.getUserTeams() != null) {
           existPlayer= user.getUserTeams()
                    .stream()
                    .filter(t->t.getTeamPlayers()!=null)
                    .map(userTeam -> userTeam.getTeamPlayers())
                    .flatMap(Collection::stream)
                    .anyMatch(userPlayer->(userPlayer.getFirstName().equalsIgnoreCase(createPlayerDTO.getFirstName())&& userPlayer.getLastName().equalsIgnoreCase(createPlayerDTO.getLastName()) &&
                            userPlayer.getBirthYear() == createPlayerDTO.getBirthYear()));
        }
//            for (Team userTeam : user.getUserTeams()) {
//                if (userTeam.getTeamPlayers() != null) {
//                    for (Player userPlayer : userTeam.getTeamPlayers()) {
//                        if (userPlayer.getFirstName().equals(createPlayerDTO.getFirstName()) &&
//                                userPlayer.getLastName().equals(createPlayerDTO.getLastName()) &&
//                                userPlayer.getBirthYear() == createPlayerDTO.getBirthYear()) {
//                            existPlayer = true;
//                        }
//                    }
//                }
//            }
//        }

        if (!existPlayer) {
            Player player = Player.builder()
                    .firstName(createPlayerDTO.getFirstName())
                    .lastName(createPlayerDTO.getLastName())
                    .nickName(createPlayerDTO.getNickName())
                    .birthYear(createPlayerDTO.getBirthYear())
                    .nationality(createPlayerDTO.getNationality())
                    .team(team)
                    .build();

            Player savedPlayer = playerRepository.save(player);
            team.getTeamPlayers().add(savedPlayer);
            return playerDTOMapper.toDto(savedPlayer);
        }
        throw new AlreadyExistsException("A player with this data is already exists!");
    }

    @Transactional
    public PlayerDTO updatePlayer(Long id, UpdatePlayerDTO updatePlayerDTO) throws NotFoundException, InvalidData, AlreadyExistsException {
        validate(updatePlayerDTO);
        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this player!"));
        Team team = player.getTeam();
        User user = team.getUser();
        boolean existPlayer=false;

        if(user.getUserTeams() != null) {
           existPlayer=user.getUserTeams()
                    .stream()
                   .filter(t -> t.getTeamPlayers()!=null)
                    .map(userTeam -> userTeam.getTeamPlayers())
                   .flatMap(Collection::stream)
                    .anyMatch(userPlayer->(userPlayer.getFirstName().equalsIgnoreCase(updatePlayerDTO.getFirstName())&& userPlayer.getLastName().equalsIgnoreCase(updatePlayerDTO.getLastName()) &&
                            userPlayer.getBirthYear() == updatePlayerDTO.getBirthYear()));
        }

        if (!existPlayer) {
            player.setFirstName(updatePlayerDTO.getFirstName());
            player.setLastName(updatePlayerDTO.getLastName());
            player.setNickName(updatePlayerDTO.getNickName());
            player.setBirthYear(updatePlayerDTO.getBirthYear());
            player.setNationality(updatePlayerDTO.getNationality());
            Player savedPlayer = playerRepository.save(player);
            return playerDTOMapper.toDto(savedPlayer);
        }
        throw new AlreadyExistsException("Player with this data is already exists!");
    }

    @Transactional
    public PlayerDTO deletedById(Long id) throws NotFoundException {
        Player existingPlayer = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this player!"));
        List<Race> playerRaces = new ArrayList<>();
        playerRaces.addAll(existingPlayer.getRedHelmetHostsPlayerRaces());
        playerRaces.addAll(existingPlayer.getBlueHelmetHostsPlayerRaces());
        playerRaces.addAll(existingPlayer.getWhiteHelmetGuestsPlayerRaces());
        playerRaces.addAll(existingPlayer.getYellowHelmetGuestsPlayerRaces());

        Iterator<Race> iteratorRaces = playerRaces.iterator();
        while (iteratorRaces.hasNext()) {
            Race race = iteratorRaces.next();
            Game game = race.getGame();
            game.getRaces().remove(race);
            raceService.deleteById(race.getId());
        }
        playerRepository.delete(existingPlayer);
        return playerDTOMapper.toDto(existingPlayer);
    }


    private void validate(CreatePlayerDTO createPlayerDTO) throws InvalidData {
        if (createPlayerDTO.getFirstName() == null || createPlayerDTO.getFirstName().isEmpty()) {
            throw new InvalidData("Player must have an non-empty first name!");
        }
        if (createPlayerDTO.getFirstName().length() < 3) {
            throw new InvalidData("First name must have a minimum of 3 characters!");
        }
        if (createPlayerDTO.getLastName() == null || createPlayerDTO.getLastName().isEmpty()) {
            throw new InvalidData("Player must have an non-empty last name!");
        }
        if (createPlayerDTO.getLastName().length() < 3) {
            throw new InvalidData("Last name must have a minimum of 3 characters!");
        }
        if (createPlayerDTO.getBirthYear() <= 1900) {
            throw new InvalidData("Birth Year must be higher than 1900");
        }
        if (createPlayerDTO.getNationality() == null || createPlayerDTO.getNationality().isEmpty()) {
            throw new InvalidData("Player must have an non-empty nationality!");
        }
    }

    private void validate(UpdatePlayerDTO updatePlayerDTO) throws InvalidData {
        if (updatePlayerDTO.getFirstName() == null || updatePlayerDTO.getFirstName().isEmpty()) {
            throw new InvalidData("Player must have an non-empty first name!");
        }
        if (updatePlayerDTO.getFirstName().length() < 3) {
            throw new InvalidData("First name must have a minimum of 3 characters!");
        }
        if (updatePlayerDTO.getLastName() == null || updatePlayerDTO.getLastName().isEmpty()) {
            throw new InvalidData("Player must have an non-empty last name!");
        }
        if (updatePlayerDTO.getLastName().length() < 3) {
            throw new InvalidData("Last name must have a minimum of 3 characters!");
        }
        if (updatePlayerDTO.getBirthYear() <= 1900) {
            throw new InvalidData("Birth Year must be higher than 1900");
        }
        if (updatePlayerDTO.getNationality() == null || updatePlayerDTO.getNationality().isEmpty()) {
            throw new InvalidData("Player must have an non-empty nationality!");
        }
    }
}