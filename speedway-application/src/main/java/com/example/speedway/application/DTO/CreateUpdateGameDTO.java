package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateUpdateGameDTO {
    @NotBlank(message = "Description must be filled")
    @Size(min = 10, max = 160, message = "Description must be at least 10 chars long and at most 160 chars")
    private String description;
    @NotNull(message = "Date must be selected")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    @NotBlank(message = "Hosts must be selected")
    private String hostsName;
    @NotBlank(message = "Guests must be selected")
    private String guestsName;
}
