package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUpdateTeamDTO;
import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.TeamDTOMapper;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.repository.TeamRepository;
import com.example.speedway.application.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UpdateTeamView {
    @Autowired
    private TeamService teamService;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TeamDTOMapper teamDTOMapper;

    @GetMapping("/update_team")
    public ModelAndView displayUpdateTeamView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("update_team_form");
        modelAndView.addObject("id", id);
        Team team = teamRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this team!"));
        TeamDTO teamDTO = teamDTOMapper.toDto(team);
        CreateUpdateTeamDTO createUpdateTeamDTO = new CreateUpdateTeamDTO();
        createUpdateTeamDTO.setName(teamDTO.getName());
        createUpdateTeamDTO.setCity(teamDTO.getCity());
        createUpdateTeamDTO.setCoach(teamDTO.getCoach());
        modelAndView.addObject("createUpdateTeamDTO", createUpdateTeamDTO);
        return modelAndView;
    }

    @PostMapping("/update_team/{id}")
    public String updateTeam(@PathVariable Long id, @Valid @ModelAttribute CreateUpdateTeamDTO createUpdateTeamDTO, BindingResult bindingResult, Model model) throws NotFoundException, InvalidData, AlreadyExistsException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUpdateTeamDTO", createUpdateTeamDTO);
            return "update_team_form";
        }
        try {
            teamService.updateTeam(id, createUpdateTeamDTO);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/team_already_exists_update_error/" + id;
        } catch (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }
        return "redirect:/team_details?id={id}";
    }

}
