package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccessDeniedView {

    @GetMapping("/access_denied")
    public ModelAndView accessDeniedView() {
        ModelAndView modelAndView = new ModelAndView("access_denied");
        return modelAndView;
    }
}
