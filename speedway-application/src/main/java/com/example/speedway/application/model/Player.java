package com.example.speedway.application.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private String nickName;
    private int birthYear;
    private String nationality;
    @ManyToOne
    private Team team;
    @OneToMany(mappedBy = "redHelmetHostsPlayer")
    private List<Race> redHelmetHostsPlayerRaces = new ArrayList<>();
    @OneToMany(mappedBy = "blueHelmetHostsPlayer")
    private List<Race> blueHelmetHostsPlayerRaces = new ArrayList<>();
    @OneToMany(mappedBy = "whiteHelmetGuestsPlayer")
    private List<Race> whiteHelmetGuestsPlayerRaces = new ArrayList<>();
    @OneToMany(mappedBy = "yellowHelmetGuestsPlayer")
    private List<Race> yellowHelmetGuestsPlayerRaces = new ArrayList<>();

}
