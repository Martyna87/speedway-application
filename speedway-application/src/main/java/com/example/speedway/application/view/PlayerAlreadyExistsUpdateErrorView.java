package com.example.speedway.application.view;

import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PlayerAlreadyExistsUpdateErrorView {
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    PlayerDTOMapper playerDTOMapper;
    @Autowired
    UserDTOMapper userDTOMapper;

    @GetMapping("/player_already_exists_update_error/{id}")
    public ModelAndView errorUpdatePlayerView(@PathVariable Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("player_already_exists_update_error");
        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Player not found!"));
        PlayerDTO playerDTO = playerDTOMapper.toDto(player);
        modelAndView.addObject("playerDTO", playerDTO);

        User user = player.getTeam().getUser();
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO", userDTO);
        return modelAndView;
    }
}
