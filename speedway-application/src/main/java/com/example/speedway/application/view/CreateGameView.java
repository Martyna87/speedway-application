package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUpdateGameDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class CreateGameView {
    @Autowired
    private GameService gameService;
    @Autowired
    private UserRepository userRepository;


    @GetMapping("/create_game")
    public ModelAndView displayCreateGameForm(Authentication authentication) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("create_game");
        String login = authentication.getName();
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User not found!"));
        CreateUpdateGameDTO createUpdateGameDTO = new CreateUpdateGameDTO();
        modelAndView.addObject("createUpdateGameDTO", createUpdateGameDTO);

        List<String> userTeamNames = new ArrayList<>();
        if (user.getUserTeams() != null) {
            userTeamNames = user.getUserTeams()
                    .stream()
                    .map(t -> t.getName())
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("userTeamNames", userTeamNames);

        return modelAndView;
    }

    @PostMapping("/create_game")
    public String createGame(@Valid @ModelAttribute CreateUpdateGameDTO createUpdateGameDTO, BindingResult bindingResult, Model model, Authentication authentication) throws NotFoundException {
        String login = authentication.getName();
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User not found!"));
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUpdateGameDTO", createUpdateGameDTO);

            List<String> userTeamNames = new ArrayList<>();
            if (user.getUserTeams() != null) {
                userTeamNames = user.getUserTeams()
                        .stream()
                        .map(t -> t.getName())
                        .collect(Collectors.toList());
            }
            model.addAttribute("userTeamNames", userTeamNames);
            return "create_game";
        }
        try {
            gameService.createGame(createUpdateGameDTO, login);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/game_already_exists_error";
        } catch (InvalidData invalidData) {
            return "redirect:/host_the_same_as_guest_error";
        }
        return "redirect:/user_games?id=" + user.getId();
    }
}
