package com.example.speedway.application.repository;

import com.example.speedway.application.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {

}
