package com.example.speedway.application.repository;

import com.example.speedway.application.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;


public interface GameRepository extends JpaRepository<Game, Long> {
}
