package com.example.speedway.application.mapper;

import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.model.Race;
import org.springframework.stereotype.Component;

@Component
public class RaceDTOMapper {
    public RaceDTO toDto(Race race) {
        Long gameId = null;
        if (race.getGame() != null) {
            gameId = race.getGame().getId();
        }

        Long redHelmetHostsPlayerId = null;
        if (race.getRedHelmetHostsPlayer() != null) {
            redHelmetHostsPlayerId = race.getRedHelmetHostsPlayer().getId();
        }

        Long blueHelmetHostsPlayerId = null;
        if (race.getBlueHelmetHostsPlayer() != null) {
            blueHelmetHostsPlayerId = race.getBlueHelmetHostsPlayer().getId();
        }

        Long whiteHelmetGuestsPlayerId = null;
        if (race.getWhiteHelmetGuestsPlayer() != null) {
            whiteHelmetGuestsPlayerId = race.getWhiteHelmetGuestsPlayer().getId();
        }

        Long yellowHelmetGuestsPlayerId = null;
        if (race.getYellowHelmetGuestsPlayer() != null) {
            yellowHelmetGuestsPlayerId = race.getYellowHelmetGuestsPlayer().getId();
        }

        return RaceDTO.builder()
                .id(race.getId())
                .number(race.getNumber())
                .redHelmetHostsPlayerId(redHelmetHostsPlayerId)
                .pointsRedHelmetHostsPlayer(race.getPointsRedHelmetHostsPlayer())
                .blueHelmetHostsPlayerId(blueHelmetHostsPlayerId)
                .pointsBlueHelmetHostsPlayer(race.getPointsBlueHelmetHostsPlayer())
                .whiteHelmetGuestsPlayerId(whiteHelmetGuestsPlayerId)
                .pointsWhiteHelmetGuestsPlayer(race.getPointsWhiteHelmetGuestsPlayer())
                .yellowHelmetGuestsPlayerId(yellowHelmetGuestsPlayerId)
                .pointsYellowHelmetGuestsPlayer(race.getPointsYellowHelmetGuestsPlayer())
                .gameId(gameId)
                .build();
    }
}
