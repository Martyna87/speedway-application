package com.example.speedway.application.view;

import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DeletePlayerStatementView {
    @Autowired
    private PlayerRepository playerRepository;

    @GetMapping("/delete_player_statement")
    public ModelAndView deletePlayerStatementView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("delete_player_statement");
        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Player not found!"));
        modelAndView.addObject("player", player);

        User user = player.getTeam().getUser();
        modelAndView.addObject("user", user);
        return modelAndView;
    }
}
