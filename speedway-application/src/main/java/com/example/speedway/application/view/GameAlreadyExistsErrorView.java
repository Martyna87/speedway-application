package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GameAlreadyExistsErrorView {

    @GetMapping("/game_already_exists_error")
    public ModelAndView errorCreateGameView() {
        ModelAndView modelAndView = new ModelAndView("game_already_exists_error");
        return modelAndView;
    }
}
