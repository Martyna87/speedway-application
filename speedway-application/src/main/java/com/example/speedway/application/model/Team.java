package com.example.speedway.application.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String city;
    private String coach;
    @OneToMany(mappedBy = "team")
    private List<Player> teamPlayers = new ArrayList<>();
    @OneToMany(mappedBy = "hosts")
    private List<Game> gamesHosts = new ArrayList<>();
    @OneToMany(mappedBy = "guests")
    private List<Game> gamesGuests = new ArrayList<>();
    @ManyToOne
    private User user;

}
