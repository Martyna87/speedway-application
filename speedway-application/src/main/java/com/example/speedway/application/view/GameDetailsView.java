package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.mapper.RaceDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

@Controller
public class GameDetailsView {
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameDTOMapper gameDTOMapper;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;
    @Autowired
    private RaceDTOMapper raceDTOMapper;


    @GetMapping("/game_details")
    public ModelAndView gameView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("game_details");
        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found game!"));
        GameDTO gameDTO = gameDTOMapper.toDto(game);
        modelAndView.addObject("gameDto", gameDTO);

        List<RaceDTO> races = new ArrayList<>();
        if (game.getRaces() != null) {
            races = game.getRaces()
                    .stream()
                    .map(race -> raceDTOMapper.toDto(race))
                    .sorted(Comparator.comparing(RaceDTO::getNumber))
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("races", races);

        List<Player> allPlayersInTeams = new ArrayList<>();
        if (game.getHosts().getTeamPlayers() != null) {
            allPlayersInTeams.addAll(game.getHosts().getTeamPlayers());
        }
        if (game.getGuests().getTeamPlayers() != null) {
            allPlayersInTeams.addAll(game.getGuests().getTeamPlayers());
        }

        Set<PlayerDTO> playerHostsSet = new HashSet<>();
        Set<PlayerDTO> playerGuestsSet = new HashSet<>();
        Map<Long, Integer> pointsPlayerInGame = new HashMap<>();

        for (Player player : allPlayersInTeams) {
            Integer sum = 0;
            for (RaceDTO raceDTO : races) {
                if (raceDTO.getRedHelmetHostsPlayerId() == player.getId()) {
                    sum += raceDTO.getPointsRedHelmetHostsPlayer();
                    playerHostsSet.add(playerDTOMapper.toDto(player));
                } else if (raceDTO.getBlueHelmetHostsPlayerId() == player.getId()) {
                    sum += raceDTO.getPointsBlueHelmetHostsPlayer();
                    playerHostsSet.add(playerDTOMapper.toDto(player));
                } else if (raceDTO.getWhiteHelmetGuestsPlayerId() == player.getId()) {
                    sum += raceDTO.getPointsWhiteHelmetGuestsPlayer();
                    playerGuestsSet.add(playerDTOMapper.toDto(player));
                } else if (raceDTO.getYellowHelmetGuestsPlayerId() == player.getId()) {
                    sum += raceDTO.getPointsYellowHelmetGuestsPlayer();
                    playerGuestsSet.add(playerDTOMapper.toDto(player));
                }
            }
            pointsPlayerInGame.put(player.getId(), sum);
        }
        modelAndView.addObject("pointsPlayerInGame", pointsPlayerInGame);

        List<PlayerDTO> playersHosts = new ArrayList<>(playerHostsSet);
        playersHosts.sort(Comparator.comparing(PlayerDTO::getLastName));
        modelAndView.addObject("playersHosts", playersHosts);

        List<PlayerDTO> playersGuests = new ArrayList<>(playerGuestsSet);
        playersGuests.sort(Comparator.comparing(PlayerDTO::getLastName));
        modelAndView.addObject("playersGuests", playersGuests);
        return modelAndView;
    }

}
