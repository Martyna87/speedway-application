package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUpdateRaceDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.mapper.RaceDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.repository.RaceRepository;
import com.example.speedway.application.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UpdateRaceView {
    @Autowired
    private RaceService raceService;
    @Autowired
    private RaceRepository raceRepository;
    @Autowired
    private RaceDTOMapper raceDTOMapper;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;

    @GetMapping("/update_race")
    public ModelAndView displayUpdateRaceView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("update_race_form");
        modelAndView.addObject("id", id);
        Race race = raceRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found race!"));
        RaceDTO raceDTO = raceDTOMapper.toDto(race);
        Game game = race.getGame();
        CreateUpdateRaceDTO createUpdateRaceDTO = new CreateUpdateRaceDTO();
        createUpdateRaceDTO.setNumber(raceDTO.getNumber());
        createUpdateRaceDTO.setRedHelmetHostsPlayerId(raceDTO.getRedHelmetHostsPlayerId());
        createUpdateRaceDTO.setPointsRedHelmetHostsPlayer(raceDTO.getPointsRedHelmetHostsPlayer());
        createUpdateRaceDTO.setBlueHelmetHostsPlayerId(raceDTO.getBlueHelmetHostsPlayerId());
        createUpdateRaceDTO.setPointsBlueHelmetHostsPlayer(raceDTO.getPointsBlueHelmetHostsPlayer());
        createUpdateRaceDTO.setWhiteHelmetGuestsPlayerId(raceDTO.getWhiteHelmetGuestsPlayerId());
        createUpdateRaceDTO.setPointsWhiteHelmetGuestsPlayer(raceDTO.getPointsWhiteHelmetGuestsPlayer());
        createUpdateRaceDTO.setYellowHelmetGuestsPlayerId(raceDTO.getYellowHelmetGuestsPlayerId());
        createUpdateRaceDTO.setPointsYellowHelmetGuestsPlayer(raceDTO.getPointsYellowHelmetGuestsPlayer());
        modelAndView.addObject("createUpdateRaceDTO", createUpdateRaceDTO);

        List<PlayerDTO> playerHosts = new ArrayList<>();
        if (game.getGuests().getTeamPlayers() != null) {
            playerHosts = game.getHosts()
                    .getTeamPlayers()
                    .stream()
                    .map(p -> playerDTOMapper.toDto(p))
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("playerHosts", playerHosts);

        List<PlayerDTO> playerGuests = new ArrayList<>();
        if (game.getGuests().getTeamPlayers() != null) {
            playerGuests = game.getGuests()
                    .getTeamPlayers()
                    .stream()
                    .map(p -> playerDTOMapper.toDto(p))
                    .collect(Collectors.toList());
        }
        modelAndView.addObject("playerGuests", playerGuests);

        List<Integer> points = new ArrayList<>();
        for (int point = 0; point < 4; point++) {
            points.add(point);
        }
        modelAndView.addObject("points", points);

        List<Integer> raceNumbers = new ArrayList<>();
        for (int raceNumber = 1; raceNumber < 16; raceNumber++) {
            raceNumbers.add(raceNumber);
        }
        modelAndView.addObject("raceNumbers", raceNumbers);
        return modelAndView;
    }

    @PostMapping("/update_race/{id}")
    public String updateRace(@PathVariable Long id, @Valid @ModelAttribute CreateUpdateRaceDTO createUpdateRaceDTO, BindingResult bindingResult, Model model) throws NotFoundException, AlreadyExistsException, InvalidData {
        Race race = raceRepository.findById(id).orElseThrow(() -> new NotFoundException("Race not found!"));
        Game game = race.getGame();
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUpdateRaceDTO", createUpdateRaceDTO);
            List<PlayerDTO> playerHosts = new ArrayList<>();
            if (game.getGuests().getTeamPlayers() != null) {
                playerHosts = game.getHosts()
                        .getTeamPlayers()
                        .stream()
                        .map(p -> playerDTOMapper.toDto(p))
                        .collect(Collectors.toList());
            }
            model.addAttribute("playerHosts", playerHosts);
            List<PlayerDTO> playerGuests = new ArrayList<>();
            if (game.getGuests().getTeamPlayers() != null) {
                playerGuests = game.getGuests()
                        .getTeamPlayers()
                        .stream()
                        .map(p -> playerDTOMapper.toDto(p))
                        .collect(Collectors.toList());
            }
            model.addAttribute("playerGuests", playerGuests);
            List<Integer> points = new ArrayList<>();
            for (int point = 0; point < 4; point++) {
                points.add(point);
            }
            model.addAttribute("points", points);
            List<Integer> raceNumbers = new ArrayList<>();
            for (int raceNumber = 1; raceNumber < 16; raceNumber++) {
                raceNumbers.add(raceNumber);
            }
            model.addAttribute("raceNumbers", raceNumbers);
            return "update_race_form";
        }
        try {
            raceService.updateRace(id, createUpdateRaceDTO);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/race_already_exists_update_error/" + race.getId();
        } catch (InvalidData invalidData) {
            return "redirect:/the_same_player_or_points_update_error/" + race.getId();
        } catch (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }

        return "redirect:/game_details?id=" + game.getId();
    }
}
