package com.example.speedway.application.repository;

import com.example.speedway.application.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {
}
