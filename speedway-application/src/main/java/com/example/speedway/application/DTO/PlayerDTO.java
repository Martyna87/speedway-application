package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlayerDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String nickName;
    private int birthYear;
    private String nationality;
    private Long teamId;
    private List<Long> redHelmetHostsPlayerRaceIds = new ArrayList<>();
    private List<Long> blueHelmetHostsPlayerRaceIds = new ArrayList<>();
    private List<Long> whiteHelmetGuestsPlayerRaceIds = new ArrayList<>();
    private List<Long> yellowHelmetGuestsPlayerRaceIds = new ArrayList<>();

}
