package com.example.speedway.application.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private boolean isAdmin = false;
    @OneToMany(mappedBy = "user")
    private List<Team> userTeams = new ArrayList<>();
    @OneToMany(mappedBy = "user")
    private List<Game> userGames = new ArrayList<>();



}


