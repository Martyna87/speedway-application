package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TeamAlreadyExistsErrorView {
    @GetMapping("/team_already_exists_error")
    public ModelAndView errorCreateTeamView() {
        ModelAndView modelAndView = new ModelAndView("team_already_exists_error");
        return modelAndView;
    }
}
