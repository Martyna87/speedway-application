package com.example.speedway.application.service;

import com.example.speedway.application.DTO.CreateUpdateGameDTO;
import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameService {
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameDTOMapper gameDTOMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RaceService raceService;


    @Transactional
    public List<GameDTO> getAllGames() {
        return gameRepository.findAll()
                .stream()
                .map(g -> gameDTOMapper.toDto(g))
                .collect(Collectors.toList());
    }

    @Transactional
    public GameDTO getGameById(Long id) throws NotFoundException {
        return gameRepository.findById(id)
                .map(g -> gameDTOMapper.toDto(g))
                .orElseThrow(() -> new NotFoundException("Not found this game!"));
    }

    @Transactional
    public GameDTO createGame(CreateUpdateGameDTO createUpdateGameDTO, String login) throws AlreadyExistsException, InvalidData, NotFoundException {
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("Not found User!"));
        validate(createUpdateGameDTO);
        Team hosts = null;
        Team guests = null;
        if (user.getUserTeams() != null) {
            for (Team team : user.getUserTeams()) {
                if (team.getName().equalsIgnoreCase(createUpdateGameDTO.getHostsName())) {
                    hosts = team;
                } else if (team.getName().equalsIgnoreCase(createUpdateGameDTO.getGuestsName())) {
                    guests = team;
                }
            }
        }

        if (createUpdateGameDTO.getHostsName().equals(createUpdateGameDTO.getGuestsName())) {
            throw new InvalidData("Guest must be different than hosts!");
        }

        boolean existGame = false;
        for (Game userGame : user.getUserGames()) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String userDate = dateTimeFormatter.format(userGame.getDate());
            String createUpdateGameDTODate = dateTimeFormatter.format(createUpdateGameDTO.getDate());
            if (userDate.equalsIgnoreCase(createUpdateGameDTODate)) {
                if (userGame.getHosts().getName().equalsIgnoreCase(createUpdateGameDTO.getHostsName()) ||
                        userGame.getGuests().getName().equalsIgnoreCase(createUpdateGameDTO.getHostsName()) ||
                        userGame.getHosts().getName().equalsIgnoreCase(createUpdateGameDTO.getGuestsName()) ||
                        userGame.getGuests().getName().equalsIgnoreCase(createUpdateGameDTO.getGuestsName())) {
                    existGame = true;
                }
            }
        }

        if (!existGame) {
            Game game = Game.builder()
                    .description(createUpdateGameDTO.getDescription())
                    .date(createUpdateGameDTO.getDate())
                    .hosts(hosts)
                    .guests(guests)
                    .user(user)
                    .build();

            Game savedGame = gameRepository.save(game);
            user.getUserGames().add(game);
            hosts.getGamesHosts().add(game);
            guests.getGamesGuests().add(game);
            return gameDTOMapper.toDto(savedGame);
        }
        throw new AlreadyExistsException("A game with this data is already exists!");
    }

    @Transactional
    public GameDTO updateGame(Long id, CreateUpdateGameDTO createUpdateGameDTO) throws NotFoundException, InvalidData, AlreadyExistsException {
        validate(createUpdateGameDTO);
        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this game!"));
        User user = game.getUser();
        Team hosts = null;
        Team guests = null;
        if (user.getUserTeams() != null) {
            for (Team team : user.getUserTeams()) {
                if (team.getName().equalsIgnoreCase(createUpdateGameDTO.getHostsName())) {
                    hosts = team;
                } else if (team.getName().equalsIgnoreCase(createUpdateGameDTO.getGuestsName())) {
                    guests = team;
                }
            }
        }
        if (createUpdateGameDTO.getHostsName().equals(createUpdateGameDTO.getGuestsName())) {
            throw new InvalidData("Guest must be different than hosts!");
        }

        boolean existGame = false;
        for (Game userGame : user.getUserGames()) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String userDate = dateTimeFormatter.format(game.getDate());
            String createUpdateGameDTODate = dateTimeFormatter.format(createUpdateGameDTO.getDate());
            if (userDate.equalsIgnoreCase(createUpdateGameDTODate)) {
                if (userGame.getHosts().getName().equalsIgnoreCase(createUpdateGameDTO.getHostsName()) &&
                        userGame.getGuests().getName().equalsIgnoreCase(createUpdateGameDTO.getGuestsName()) &&
                        userGame.getDescription().equalsIgnoreCase(createUpdateGameDTO.getDescription()) ||
                        !game.getHosts().getName().equalsIgnoreCase(createUpdateGameDTO.getHostsName()) &&
                                !game.getGuests().getName().equalsIgnoreCase(createUpdateGameDTO.getGuestsName()) &&
                                !userDate.equalsIgnoreCase(createUpdateGameDTODate)) {
                    existGame = true;
                }
            }
        }

        if (!existGame) {
            game.setDescription(createUpdateGameDTO.getDescription());
            game.setDate(createUpdateGameDTO.getDate());
            game.setHosts(hosts);
            game.setGuests(guests);

            Game savedGame = gameRepository.save(game);
            return gameDTOMapper.toDto(savedGame);
        }
        throw new AlreadyExistsException("Game with this data is already exists!");
    }


    @Transactional
    public GameDTO deleteById(Long id) throws NotFoundException {
        Game existsGame = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this game!"));
        User user = existsGame.getUser();
        user.getUserGames().remove(existsGame);
        Team hosts = existsGame.getHosts();
        Team guests = existsGame.getGuests();
        hosts.getGamesHosts().remove(existsGame);
        guests.getGamesGuests().remove(existsGame);
        List<Race> gameRaces = existsGame.getRaces();
        Iterator<Race> iteratorGameRaces = gameRaces.iterator();
        while (iteratorGameRaces.hasNext()) {
            Race race = iteratorGameRaces.next();
            iteratorGameRaces.remove();
            raceService.deleteById(race.getId());
        }
        gameRepository.delete(existsGame);
        return gameDTOMapper.toDto(existsGame);
    }


    private void validate(CreateUpdateGameDTO createUpdateGameDTO) throws InvalidData {
        if (createUpdateGameDTO.getDescription().length() < 10) {
            throw new InvalidData("Description must have a minimum of 10 characters!");
        }
        if (createUpdateGameDTO.getDate() == null) {
            throw new InvalidData("Game must have an non-empty last date!");
        }
        if (createUpdateGameDTO.getHostsName() == null || createUpdateGameDTO.getHostsName().isEmpty()) {
            throw new InvalidData("Game must have an non-empty hosts!");
        }
        if (createUpdateGameDTO.getGuestsName() == null || createUpdateGameDTO.getGuestsName().isEmpty()) {
            throw new InvalidData("Game must have an non-empty guests!");
        }

    }
}
