package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GameAlreadyExistsUpdateErrorView {
    @Autowired
    GameRepository gameRepository;
    @Autowired
    GameDTOMapper gameDTOMapper;
    @Autowired
    UserDTOMapper userDTOMapper;

    @GetMapping("/game_already_exists_update_error/{id}")
    public ModelAndView errorUpdateGameView(@PathVariable Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("game_already_exists_update_error");
        Game game = gameRepository.findById(id).orElseThrow(() -> new NotFoundException("Game not found!"));
        GameDTO gameDTO = gameDTOMapper.toDto(game);
        modelAndView.addObject("gameDTO", gameDTO);

        User user = game.getUser();
        UserDTO userDTO = userDTOMapper.toDto(user);
        modelAndView.addObject("userDTO", userDTO);
        return modelAndView;
    }

}
