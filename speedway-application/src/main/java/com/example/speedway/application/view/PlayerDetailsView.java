package com.example.speedway.application.view;


import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.mapper.RaceDTOMapper;
import com.example.speedway.application.model.*;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.repository.PlayerRepository;
import com.example.speedway.application.repository.RaceRepository;
import com.example.speedway.application.service.GameService;
import com.example.speedway.application.service.PlayerService;
import com.example.speedway.application.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

@Controller
public class PlayerDetailsView {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PlayerDTOMapper playerDTOMapper;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RaceService raceService;
    @Autowired
    private GameService gameService;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameDTOMapper gameDTOMapper;
    @Autowired
    private RaceRepository raceRepository;
    @Autowired
    private RaceDTOMapper raceDTOMapper;


    @GetMapping("/player_details")
    public ModelAndView playerView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("player_details");
        Player player = playerRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found player!"));
        PlayerDTO playerDTO = playerDTOMapper.toDto(player);
        modelAndView.addObject("playerDto", playerDTO);

        User user = player.getTeam().getUser();
        List<Team> userTeams = new ArrayList<>();
        if (user.getUserTeams() != null) {
            userTeams = user.getUserTeams();
        }
        modelAndView.addObject("userTeams", userTeams);

        List<Race> playerRacesList = new ArrayList<>();
        playerRacesList.addAll(player.getRedHelmetHostsPlayerRaces());
        playerRacesList.addAll(player.getBlueHelmetHostsPlayerRaces());
        playerRacesList.addAll(player.getWhiteHelmetGuestsPlayerRaces());
        playerRacesList.addAll(player.getYellowHelmetGuestsPlayerRaces());
        List<RaceDTO> playerRaces = playerRacesList
                .stream()
                .map(race -> raceDTOMapper.toDto(race))
                .collect(Collectors.toList());
        modelAndView.addObject("playerRaces", playerRaces);

        List<PlayerDTO> allPlayers = playerService.getAllPlayers();
        modelAndView.addObject("allPlayers", allPlayers);

        List<GameDTO> allGames = gameService.getAllGames();
        modelAndView.addObject("allGames", allGames);

        Set<GameDTO> playerGames = new HashSet<>();
        for (RaceDTO raceDTO : playerRaces) {
            Game game = gameRepository.findById(raceDTO.getGameId()).orElseThrow(() -> new NotFoundException("Game not found!"));
            playerGames.add(gameDTOMapper.toDto(game));
        }
        modelAndView.addObject("playerGames", playerGames);

        Map<Long, Integer> pointsInGame = new HashMap<>();
        for (GameDTO gameDTO : playerGames) {
            Integer sum = 0;
            for (Long raceId : gameDTO.getRaceIds()) {
                Race race = raceRepository.findById(raceId).orElseThrow(() -> new NotFoundException("Race not found!"));
                if (race.getRedHelmetHostsPlayer().getId() == player.getId()) {
                    sum += race.getPointsRedHelmetHostsPlayer();
                } else if (race.getBlueHelmetHostsPlayer().getId() == player.getId()) {
                    sum += race.getPointsBlueHelmetHostsPlayer();
                } else if (race.getWhiteHelmetGuestsPlayer().getId() == player.getId()) {
                    sum += race.getPointsWhiteHelmetGuestsPlayer();
                } else if (race.getYellowHelmetGuestsPlayer().getId() == player.getId()) {
                    sum += race.getPointsYellowHelmetGuestsPlayer();
                }
            }
            pointsInGame.put(gameDTO.getId(), sum);
        }
        modelAndView.addObject("pointsInGame", pointsInGame);

        Set<Map.Entry<Long, Integer>> values = pointsInGame.entrySet();
        Integer sumInAllGames = 0;
        for (Map.Entry<Long, Integer> value : values) {
            sumInAllGames += value.getValue();
        }
        double averagePerGame = (double) sumInAllGames / (double) playerGames.size();
        java.text.DecimalFormat df = new java.text.DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        modelAndView.addObject("sumPlayerPoints", sumInAllGames);
        modelAndView.addObject("averagePerGame", df.format(averagePerGame));


        double averagePerRace = (double) sumInAllGames / (double) playerRaces.size();
        modelAndView.addObject("averagePerRace", df.format(averagePerRace));
        return modelAndView;
    }
}
