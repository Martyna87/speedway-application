package com.example.speedway.application.view;

import com.example.speedway.application.DTO.CreateUpdateTeamDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CreateTeamView {
    @Autowired
    private TeamService teamService;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/create_team")
    public ModelAndView displayCreateTeamForm() {
        ModelAndView modelAndView = new ModelAndView("create_team");
        CreateUpdateTeamDTO createUpdateTeamDTO = new CreateUpdateTeamDTO();
        modelAndView.addObject("createUpdateTeamDTO", createUpdateTeamDTO);
        return modelAndView;
    }

    @PostMapping("/create_team")
    public String createTeam(@Valid @ModelAttribute CreateUpdateTeamDTO createUpdateTeamDTO, BindingResult bindingResult, Model model, Authentication authentication) throws InvalidData, NotFoundException {
        String login = authentication.getName();
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User not found!"));
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUpdateTeamDTO", createUpdateTeamDTO);
            return "create_team";
        }
        try {
            teamService.createTeam(createUpdateTeamDTO, login);
        } catch (AlreadyExistsException alreadyExists) {
            return "redirect:/team_already_exists_error";
        }
        return "redirect:/user_teams?id=" + user.getId();
    }


}
