package com.example.speedway.application.view;


import com.example.speedway.application.DTO.UpdateUserDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UpdateUserView {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDTOMapper userDTOMapper;
    @Autowired
    private UserService userService;

    @GetMapping("/update_user")
    public ModelAndView displayUpdateUserView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("update_user_form");
        modelAndView.addObject("id", id);

        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this user!"));
        UserDTO userDTO = userDTOMapper.toDto(user);
        UpdateUserDTO updateUserDTO = new UpdateUserDTO();
        updateUserDTO.setPassword("");
        updateUserDTO.setRePassword("");
        updateUserDTO.setFirstName(userDTO.getFirstName());
        updateUserDTO.setLastName(userDTO.getLastName());
        updateUserDTO.setEmail(userDTO.getEmail());
        updateUserDTO.setCity(userDTO.getCity());
        modelAndView.addObject("updateUserDTO", updateUserDTO);

        return modelAndView;
    }

    @PostMapping("/update_user/{id}")
    public String updateUser(@PathVariable Long id, @Valid @ModelAttribute UpdateUserDTO updateUserDTO, BindingResult bindingResult, Model model) throws NotFoundException {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found!"));
        if (bindingResult.hasErrors()) {
            model.addAttribute("updateUserDTO", updateUserDTO);
            return "update_user_form";
        }
        try {
            userService.updateUser(updateUserDTO, id);
        } catch (InvalidData invalidData) {
            return "redirect:/different_passwords_update_error/" + user.getId();
        } catch (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }
        return "redirect:/user_profile?id={id}";
    }
}
