package com.example.speedway.application.controller;

import com.example.speedway.application.DTO.CreateUpdateGameDTO;
import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/game")
public class GameController {
    @Autowired
    private GameService gameService;

    @GetMapping
    public List<GameDTO> getAllGames() {
        return gameService.getAllGames();
    }

    @GetMapping("/{id}")
    public GameDTO getById(@PathVariable Long id) throws NotFoundException {
        return gameService.getGameById(id);
    }

    @PostMapping
    public GameDTO createGame(@Valid @RequestBody CreateUpdateGameDTO createUpdateGameDTO, String login) throws AlreadyExistsException, InvalidData, NotFoundException {
        return gameService.createGame(createUpdateGameDTO, login);
    }

    @PutMapping("/{id}")
    public GameDTO updateGame(@Valid @RequestBody CreateUpdateGameDTO createUpdateGameDTO, @PathVariable Long id) throws InvalidData, NotFoundException, AlreadyExistsException {
        return gameService.updateGame(id, createUpdateGameDTO);
    }

    @DeleteMapping("/{id}")
    public GameDTO deleteGame(@PathVariable Long id) throws NotFoundException {
        return gameService.deleteById(id);
    }
}
