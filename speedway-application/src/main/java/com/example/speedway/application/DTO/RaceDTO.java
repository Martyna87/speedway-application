package com.example.speedway.application.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RaceDTO {
    private Long id;
    private int number;
    private Long redHelmetHostsPlayerId;
    private int pointsRedHelmetHostsPlayer;
    private Long blueHelmetHostsPlayerId;
    private int pointsBlueHelmetHostsPlayer;
    private Long whiteHelmetGuestsPlayerId;
    private int pointsWhiteHelmetGuestsPlayer;
    private Long yellowHelmetGuestsPlayerId;
    private int pointsYellowHelmetGuestsPlayer;
    private Long gameId;
}
