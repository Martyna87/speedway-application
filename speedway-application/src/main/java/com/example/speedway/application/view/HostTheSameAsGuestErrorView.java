package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HostTheSameAsGuestErrorView {
    @GetMapping("/host_the_same_as_guest_error")
    public ModelAndView errorHostTheSameAsGuestView() {
        ModelAndView modelAndView = new ModelAndView("host_the_same_as_guest_error");
        return modelAndView;
    }
}
