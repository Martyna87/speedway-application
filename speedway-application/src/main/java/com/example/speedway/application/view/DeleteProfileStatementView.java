package com.example.speedway.application.view;

import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DeleteProfileStatementView {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/delete_profile_statement")
    public ModelAndView deleteProfileStatementView(@RequestParam Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("delete_profile_statement");
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found!"));
        modelAndView.addObject("user", user);
        return modelAndView;
    }
}
