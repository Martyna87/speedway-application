package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TheSamePlayerOrPointsErrorView {
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private GameDTOMapper gameDTOMapper;

    @GetMapping("/the_same_player_or_points_error/{gameId}")
    public ModelAndView errorTheSamePlayerOrPointsView(@PathVariable Long gameId) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("the_same_player_or_points_error");
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new NotFoundException("Game not found!"));
        GameDTO gameDTO = gameDTOMapper.toDto(game);
        modelAndView.addObject("gameDTO", gameDTO);
        return modelAndView;
    }
}
