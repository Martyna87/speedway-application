package com.example.speedway.application.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Race {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int number;
    @ManyToOne
    private Player redHelmetHostsPlayer;
    private int pointsRedHelmetHostsPlayer;
    @ManyToOne
    private Player blueHelmetHostsPlayer;
    private int pointsBlueHelmetHostsPlayer;
    @ManyToOne
    private Player whiteHelmetGuestsPlayer;
    private int pointsWhiteHelmetGuestsPlayer;
    @ManyToOne
    private Player yellowHelmetGuestsPlayer;
    private int pointsYellowHelmetGuestsPlayer;
    @ManyToOne
    private Game game;
}
