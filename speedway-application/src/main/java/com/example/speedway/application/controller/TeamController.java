package com.example.speedway.application.controller;

import com.example.speedway.application.DTO.CreateUpdateTeamDTO;
import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/team")
public class TeamController {
    @Autowired
    private TeamService teamService;

    @GetMapping
    public List<TeamDTO> getAllTeams() {
        return teamService.getAllTeams();
    }

    @GetMapping("/{id}")
    public TeamDTO getById(@PathVariable Long id) throws NotFoundException {
        return teamService.getTeamById(id);
    }

    @PostMapping
    public TeamDTO createTeam(@Valid @RequestBody CreateUpdateTeamDTO createUpdateTeamDTO, String login) throws NotFoundException, AlreadyExistsException, InvalidData {
        return teamService.createTeam(createUpdateTeamDTO, login);
    }

    @PutMapping("/{id}")
    public TeamDTO updateTeam(@Valid @RequestBody CreateUpdateTeamDTO createUpdateTeamDTO, @PathVariable Long id) throws InvalidData, AlreadyExistsException, NotFoundException {
        return teamService.updateTeam(id, createUpdateTeamDTO);
    }

    @DeleteMapping("/{id}")
    public TeamDTO deleteTeam(@PathVariable Long id) throws NotFoundException {
        return teamService.deletedById(id);
    }
}
