package com.example.speedway.application.view;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.mapper.RaceDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.repository.RaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RaceAlreadyExistsUpdateErrorView {
    @Autowired
    RaceRepository raceRepository;
    @Autowired
    RaceDTOMapper raceDTOMapper;
    @Autowired
    GameDTOMapper gameDTOMapper;

    @GetMapping("/race_already_exists_update_error/{id}")
    public ModelAndView errorUpdateRaceView(@PathVariable Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("race_already_exists_update_error");
        Race race = raceRepository.findById(id).orElseThrow(() -> new NotFoundException("Race not found!"));
        RaceDTO raceDTO = raceDTOMapper.toDto(race);
        modelAndView.addObject("raceDTO", raceDTO);

        Game game = race.getGame();
        GameDTO gameDTO = gameDTOMapper.toDto(game);
        modelAndView.addObject("gameDTO", gameDTO);
        return modelAndView;
    }
}

