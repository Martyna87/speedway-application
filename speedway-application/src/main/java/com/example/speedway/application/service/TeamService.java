package com.example.speedway.application.service;

import com.example.speedway.application.DTO.CreateUpdateTeamDTO;
import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.TeamDTOMapper;
import com.example.speedway.application.model.*;
import com.example.speedway.application.repository.GameRepository;
import com.example.speedway.application.repository.TeamRepository;
import com.example.speedway.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TeamDTOMapper teamDTOMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private GameRepository gameRepository;


    @Transactional
    public List<TeamDTO> getAllTeams() {
        return teamRepository.findAll()
                .stream()
                .map(t -> teamDTOMapper.toDto(t))
                .collect(Collectors.toList());
    }

    @Transactional
    public TeamDTO getTeamById(Long id) throws NotFoundException {
        return teamRepository.findById(id)
                .map(t -> teamDTOMapper.toDto(t))
                .orElseThrow(() -> new NotFoundException("Not found team!"));
    }

    @Transactional
    public TeamDTO createTeam(CreateUpdateTeamDTO createUpdateTeamDTO, String login) throws InvalidData, AlreadyExistsException, NotFoundException {
        validate(createUpdateTeamDTO);
        User user = userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("Not found User!"));
        boolean existTeam = false;
        for (Team userTeam : user.getUserTeams()) {
            if (userTeam.getName().equalsIgnoreCase(createUpdateTeamDTO.getName())) {
                existTeam = true;
            }
        }

        if (!existTeam) {
            Team team = Team.builder()
                    .name(createUpdateTeamDTO.getName())
                    .city(createUpdateTeamDTO.getCity())
                    .coach(createUpdateTeamDTO.getCoach())
                    .user(user)
                    .build();

            Team savedTeam = teamRepository.save(team);
            user.getUserTeams().add(team);
            return teamDTOMapper.toDto(savedTeam);
        }
        throw new AlreadyExistsException("A team with this data is already exists!");
    }

    @Transactional
    public TeamDTO updateTeam(Long id, CreateUpdateTeamDTO createUpdateTeamDTO) throws NotFoundException, InvalidData, AlreadyExistsException {
        validate(createUpdateTeamDTO);
        Team team = teamRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found this team!"));
        User user = team.getUser();
        boolean existTeam = false;
        if (user.getUserTeams() != null) {
            for (Team userTeam : user.getUserTeams()) {
                if (userTeam.getName().equalsIgnoreCase(createUpdateTeamDTO.getName())) {
                    if (userTeam.getCity().equalsIgnoreCase(createUpdateTeamDTO.getCity()) &&
                            userTeam.getCoach().equalsIgnoreCase(createUpdateTeamDTO.getCoach()) ||
                            !team.getName().equalsIgnoreCase(createUpdateTeamDTO.getName())) {
                        existTeam = true;
                    }
                }
            }
        }

        if (!existTeam) {
            team.setName(createUpdateTeamDTO.getName());
            team.setCity(createUpdateTeamDTO.getCity());
            team.setCoach(createUpdateTeamDTO.getCoach());

            Team savedTeam = teamRepository.save(team);
            return teamDTOMapper.toDto(savedTeam);
        }
        throw new AlreadyExistsException("A team with this data is already exists!");
    }

    @Transactional
    public TeamDTO deletedById(Long id) throws NotFoundException {
        Team existingTeam = teamRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found team!"));
        User user = existingTeam.getUser();
        user.getUserTeams().remove(existingTeam);
        List<Player> teamPlayers = existingTeam.getTeamPlayers();
        Iterator<Player> iteratorTeamPlayers = teamPlayers.iterator();
        while (iteratorTeamPlayers.hasNext()) {
            Player player = iteratorTeamPlayers.next();
            iteratorTeamPlayers.remove();
            playerService.deletedById(player.getId());
        }
        List<Game> teamGames = new ArrayList<>();
        teamGames.addAll(existingTeam.getGamesHosts());
        teamGames.addAll(existingTeam.getGamesGuests());
        Iterator<Game> gameIterator = teamGames.iterator();
        if (gameIterator.hasNext()) {
            Game game = gameIterator.next();
            gameIterator.remove();
            gameRepository.deleteById(game.getId());
        }
        teamRepository.delete(existingTeam);
        return teamDTOMapper.toDto(existingTeam);
    }

    private void validate(CreateUpdateTeamDTO createUpdateTeamDTO) throws InvalidData {
        if (createUpdateTeamDTO.getName() == null || createUpdateTeamDTO.getName().isEmpty()) {
            throw new InvalidData("Team must have an non-empty name!");
        }
        if (createUpdateTeamDTO.getName().length() < 10) {
            throw new InvalidData("Team name must have a minimum of 10 characters!");
        }
        if (createUpdateTeamDTO.getCity() == null || createUpdateTeamDTO.getCity().isEmpty()) {
            throw new InvalidData("Team must have non-empty city!");
        }
        if (createUpdateTeamDTO.getCity().length() < 3) {
            throw new InvalidData("Team city must have a minimum of 3 characters!");
        }
        if (createUpdateTeamDTO.getCoach() == null || createUpdateTeamDTO.getCoach().isEmpty()) {
            throw new InvalidData("Team must have non-empty coach!");
        }
        if (createUpdateTeamDTO.getCoach().length() < 3) {
            throw new InvalidData("Team coach must have a minimum of 3 characters!");
        }
    }
}
