package com.example.speedway.application.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginErrorView {
    @PostMapping("/login_error")
    public ModelAndView loginErrorView() {
        ModelAndView modelAndView = new ModelAndView("login_error");
        return modelAndView;
    }
}
