package com.example.speedway.application.services;


import com.example.speedway.application.DTO.CreateUserDTO;
import com.example.speedway.application.DTO.UpdateUserDTO;
import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.exceptions.AlreadyExistsException;
import com.example.speedway.application.exceptions.InvalidData;
import com.example.speedway.application.exceptions.NotFoundException;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.User;
import com.example.speedway.application.repository.UserRepository;
import com.example.speedway.application.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserDTOMapper userDTOMapper;

    @InjectMocks
    private UserService userService;

    @Captor
    private ArgumentCaptor<Long> argumentCaptor;

    @Test
    public void getAllUsers_shouldReturnDtoEmptyList() throws NotFoundException {
        when(userRepository.findAll()).thenReturn(new ArrayList<>());
        List<UserDTO> allUsers = userService.getAllUsers();
        assertThat(allUsers.size()).isEqualTo(0);
    }

    @Test
    public void getUserById_shouldThrowUserNotFoundException() {
        assertThrows(NotFoundException.class, () -> userService.getUserById((long) 5234));
    }

    @Test
    public void getUserById_shouldReturnUserDTO() throws NotFoundException {
        User exampleUser = User.builder().login("adam").build();
        UserDTO exampleUserDTO = userDTOMapper.toDto(exampleUser);
        when(userService.getUserById((long) 123)).thenReturn(exampleUserDTO);
        UserDTO userDTO = userService.getUserById((long) 123);
        assertThat(userDTO.getLogin()).isEqualTo("adam");
    }

    @Test
    public void getUserByLogin_shouldReturnUserDTO() throws NotFoundException {
        User exampleUser = User.builder().login("adam").build();
        when(userRepository.findByLogin("adam")).thenReturn(Optional.of(exampleUser));
        UserDTO userDTO = userService.getUserByLogin("adam");
        assertThat(userDTO.getLogin()).isEqualTo("adam");
    }

    @Test
    public void createNewUser_shouldThrowUserAlreadyExists() {
        CreateUserDTO createUserDTO = new CreateUserDTO("adam","adamadam123","adamadam123","Adam","Adamczewski","adam.adamczewski@wp.pl","Kraków");
        when(userRepository.existsByLogin("adam")).thenReturn(true);
        assertThrows(AlreadyExistsException.class, () -> userService.createUser(createUserDTO));
        verify(userRepository, times(0)).save(any(User.class));
    }

    @Test
    public void createNewUser_shouldCreateNewUser() throws AlreadyExistsException,InvalidData {
        CreateUserDTO createUserDTO = new CreateUserDTO("adam","adamadam123","adamadam123","Adam","Adamczewski","adam.adamczewski@wp.pl","Kraków");
        when(userRepository.existsByLogin("adam")).thenReturn(false);
        userService.createUser(createUserDTO);
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void updateUser_shouldUpdateUser() throws InvalidData, NotFoundException{
        User user = new User((long)123,"adam","adamadam23","Adam","Adamczewski","adam.adamczewski@wp.pl","Kraków",false,new ArrayList<>(),new ArrayList<>());
        when(userRepository.findById((long)123)).thenReturn(Optional.of(user));
        UpdateUserDTO updateUserDTO =  new UpdateUserDTO("adamadam321","adamadam321","Piotr","Nowak","piotr.nowak@wp.pl","Warszawa");
        userService.updateUser(updateUserDTO,user.getId());
        assertThat(user.getPassword()).isEqualTo(updateUserDTO.getPassword());
        assertThat(user.getFirstName()).isEqualTo(updateUserDTO.getFirstName());
        assertThat(user.getLastName()).isEqualTo(updateUserDTO.getLastName());
        assertThat(user.getEmail()).isEqualTo(updateUserDTO.getEmail());
        assertThat(user.getCity()).isEqualTo(updateUserDTO.getCity());
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void deleteUser_shouldDeleteUserById() throws NotFoundException {
        User user = new User((long)123,"adam","adamadam23","Adam","Adamczewski","adam.adamczewski@wp.pl","Kraków",false,new ArrayList<>(),new ArrayList<>());
        when(userRepository.findById((long)123)).thenReturn(Optional.of(user));
        userService.deletedById(user.getId());
        verify(userRepository,times(1)).delete(any(User.class));
    }

    @Test
    public void testArgumentCaptor() throws NotFoundException {
        User user = new User((long)123,"adam","adamadam23","Adam","Adamczewski","adam.adamczewski@wp.pl","Kraków",false,new ArrayList<>(),new ArrayList<>());
        when(userRepository.findById(argumentCaptor.capture())).thenReturn(Optional.of(user));
        UserDTO userDTO = userService.getUserById((long) 123);
        System.out.println(userDTO.getId());
        userService.getUserById((long) 123);
        assertThat(argumentCaptor.getValue()).isEqualTo(123);


    }
}
