package com.example.speedway.application.DTOmappers;

import com.example.speedway.application.DTO.RaceDTO;
import com.example.speedway.application.mapper.RaceDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.Race;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class RaceDTOMapperTest {
    @InjectMocks
    private RaceDTOMapper raceDTOMapper;

    @Test
    public void shouldCreateRaceDTO() {
        Player redHelmetHostsPlayer = Player.builder().id((long)123).build();
        Player blueHelmetHostsPlayer = Player.builder().id((long)456).build();
        Player whiteHelmetGuestsPlayer = Player.builder().id((long)789).build();
        Player yellowHelmetGuestsPlayer = Player.builder().id((long)321).build();
        Game game = Game.builder().id((long)987).build();
        Race race = new Race((long)999,1,redHelmetHostsPlayer,3,blueHelmetHostsPlayer,2,whiteHelmetGuestsPlayer,1,yellowHelmetGuestsPlayer,0,game);
        RaceDTO raceDTO = raceDTOMapper.toDto(race);

        assertThat(raceDTO.getId()).isEqualTo((long)999);
        assertThat(raceDTO.getNumber()).isEqualTo(1);
        assertThat(raceDTO.getRedHelmetHostsPlayerId()).isEqualTo((long)123);
        assertThat(raceDTO.getPointsRedHelmetHostsPlayer()).isEqualTo(3);
        assertThat(raceDTO.getBlueHelmetHostsPlayerId()).isEqualTo((long)456);
        assertThat(raceDTO.getPointsBlueHelmetHostsPlayer()).isEqualTo(2);
        assertThat(raceDTO.getWhiteHelmetGuestsPlayerId()).isEqualTo((long)789);
        assertThat(raceDTO.getPointsWhiteHelmetGuestsPlayer()).isEqualTo(1);
        assertThat(raceDTO.getYellowHelmetGuestsPlayerId()).isEqualTo((long)321);
        assertThat(raceDTO.getPointsYellowHelmetGuestsPlayer()).isEqualTo(0);
        assertThat(raceDTO.getGameId()).isEqualTo(987);
    }
}
