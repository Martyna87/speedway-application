package com.example.speedway.application.DTOmappers;

import com.example.speedway.application.DTO.GameDTO;
import com.example.speedway.application.mapper.GameDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class GameDTOMapperTest {
    @InjectMocks
    private GameDTOMapper gameDTOMapper;

    @Test
    public void shouldCreateGameDTO() {
        Team hosts = Team.builder().name("Betard Sparta Wrocław").build();
        Team guests = Team.builder().name("Unia Leszno").build();
        User user = User.builder().login("adam123").build();
        List<Race> races = new ArrayList<>();
        LocalDate localDate = LocalDate.now();

        Game game = new Game((long)123,"Mecz finałowy sezonu 2019/20",localDate,hosts,36,guests,55,races,user );
        GameDTO gameDTO = gameDTOMapper.toDto(game);

        assertThat(gameDTO.getId()).isEqualTo((long)123);
        assertThat(gameDTO.getDescription()).isEqualTo("Mecz finałowy sezonu 2019/20");
        assertThat(gameDTO.getDate()).isEqualTo(localDate);
        assertThat(gameDTO.getHostsName()).isEqualTo("Betard Sparta Wrocław");
        assertThat(gameDTO.getHostPoints()).isEqualTo(36);
        assertThat(gameDTO.getGuestsName()).isEqualTo("Unia Leszno");
        assertThat(gameDTO.getGuestPoints()).isEqualTo(55);
        assertThat(gameDTO.getRaceIds()).isNullOrEmpty();
        assertThat(gameDTO.getUserLogin()).isEqualTo("adam123");
    }
}
