package com.example.speedway.application.DTOmappers;

import com.example.speedway.application.DTO.UserDTO;
import com.example.speedway.application.mapper.UserDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class UserDTOMapperTest {
    @InjectMocks
    private UserDTOMapper userDTOMapper;

    @Test
    public void shouldCreateUserDTO() {
        List<Team> userTeams = new ArrayList<>();
        List<Game> userGames = new ArrayList<>();
        User user = new User((long)123,"adam78","adamadamadam","Adam","Nowak","adamnowak@wp.pl","Kraków",false,userTeams,userGames);
        UserDTO userDTO = userDTOMapper.toDto(user);

        assertThat(userDTO.getId()).isEqualTo(123);
        assertThat(userDTO.getLogin()).isEqualTo("adam78");
        assertThat(userDTO.getFirstName()).isEqualTo("Adam");
        assertThat(userDTO.getLastName()).isEqualTo("Nowak");
        assertThat(userDTO.getEmail()).isEqualTo("adamnowak@wp.pl");
        assertThat(userDTO.isAdmin()).isEqualTo(false);
        assertThat(userDTO.getUserTeamsIds()).isNullOrEmpty();
        assertThat(userDTO.getUserGamesIds()).isNullOrEmpty();
    }

}
