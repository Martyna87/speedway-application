package com.example.speedway.application.DTOmappers;

import com.example.speedway.application.DTO.PlayerDTO;
import com.example.speedway.application.mapper.PlayerDTOMapper;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.Race;
import com.example.speedway.application.model.Team;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class PlayerDTOMapperTest {
    @InjectMocks
    private PlayerDTOMapper playerDTOMapper;

    @Test
    public void shouldCreatePlayerDTO() {
        Team team = Team.builder().id((long)345).build();
        List<Race> redHelmetHostsPlayerRaces = new ArrayList<>();
        List<Race> blueHelmetHostsPlayerRaces = new ArrayList<>();
        List<Race> whiteHelmetGuestsPlayerRaces = new ArrayList<>();
        List<Race> yellowHelmetGuestsPlayerRaces = new ArrayList<>();

        Player player = new Player((long)123,"Krzysztof","Krzysztofowski","Krzychu",1996,"Polska",team,redHelmetHostsPlayerRaces,blueHelmetHostsPlayerRaces,whiteHelmetGuestsPlayerRaces,yellowHelmetGuestsPlayerRaces);
        PlayerDTO playerDTO = playerDTOMapper.toDto(player);

        assertThat(playerDTO.getId()).isEqualTo((long)123);
        assertThat(playerDTO.getFirstName()).isEqualTo("Krzysztof");
        assertThat(playerDTO.getLastName()).isEqualTo("Krzysztofowski");
        assertThat(playerDTO.getNickName()).isEqualTo("Krzychu");
        assertThat(playerDTO.getBirthYear()).isEqualTo(1996);
        assertThat(playerDTO.getNationality()).isEqualTo("Polska");
        assertThat(playerDTO.getTeamId()).isEqualTo((long)345);
        assertThat(playerDTO.getRedHelmetHostsPlayerRaceIds()).isNullOrEmpty();
        assertThat(playerDTO.getBlueHelmetHostsPlayerRaceIds()).isNullOrEmpty();
        assertThat(playerDTO.getWhiteHelmetGuestsPlayerRaceIds()).isNullOrEmpty();
        assertThat(playerDTO.getYellowHelmetGuestsPlayerRaceIds()).isNullOrEmpty();
    }





}
