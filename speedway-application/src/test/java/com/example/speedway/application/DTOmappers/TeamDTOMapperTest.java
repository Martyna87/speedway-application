package com.example.speedway.application.DTOmappers;

import com.example.speedway.application.DTO.TeamDTO;
import com.example.speedway.application.mapper.TeamDTOMapper;
import com.example.speedway.application.model.Game;
import com.example.speedway.application.model.Player;
import com.example.speedway.application.model.Team;
import com.example.speedway.application.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class TeamDTOMapperTest {
    @InjectMocks
    private TeamDTOMapper teamDTOMapper;

    @Test
    public void shouldCreateTeamDTO() {
        List<Player> teamPlayers = new ArrayList<>();
        List<Game> gamesHosts = new ArrayList<>();
        List<Game> gamesGuests = new ArrayList<>();
        User user = User.builder().login("kasia123").build();
        Team team = new Team((long)321,"Betard Sparta Wrocław","Wrocław","Dariusz Śledź",teamPlayers,gamesHosts,gamesGuests,user);
        TeamDTO teamDTO = teamDTOMapper.toDto(team);

        assertThat(teamDTO.getId()).isEqualTo((long)321);
        assertThat(teamDTO.getName()).isEqualTo("Betard Sparta Wrocław");
        assertThat(teamDTO.getCity()).isEqualTo("Wrocław");
        assertThat(teamDTO.getCoach()).isEqualTo("Dariusz Śledź");
        assertThat(teamDTO.getTeamPlayersIds()).isNullOrEmpty();
        assertThat(teamDTO.getGameHostsIds()).isNullOrEmpty();
        assertThat(teamDTO.getGameGuestsIds()).isNullOrEmpty();
        assertThat(teamDTO.getUserLogin()).isEqualTo("kasia123");
    }
}
